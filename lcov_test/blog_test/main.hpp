#include <cstdbool>
#include <exception>
#include <map>
#include <queue>
#include <set>
using namespace std;

void EmplaceMap(int key, int value)
{
    static std::map<int, std::set<int>> mapValue;
    mapValue[key].emplace(value);
}

bool IsAbsFunc(int a, int b, bool isAbs)
{
    // 当a为100的时候抛出异常
    if (a == 100)
    {
        throw std::invalid_argument("a should not be 100.");
    }
    return isAbs;
}

// 相减函数，默认是A-B，第三个参数为是否要返回绝对值
int Sub(int a, int b, bool isAbs)
{
    if (b > a && IsAbsFunc(a, b, isAbs))
    {
        return b - a;
    }
    return a - b;
}

void CallSub()
{
    (void)Sub(1,2,false);
}

void QueueTest()
{
    std::queue<int> que;
    que.push(1);

    que = {};
}

// source code
void test_string_plus(const string& local ,const string& remote)
{
    static string recv_msg;
    static_cast<void>(recv_msg.assign("/" + (local < remote ? local + "_" + remote : remote + "_" + local)));
}
