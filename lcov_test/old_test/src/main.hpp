#pragma once
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <new>
#include <set>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;

// // 你要测试的普通函数
// int Add(int a, int b) {
//     return a + b;
// }

// 相减函数
int Sub(int a, int b, int c = 10)
{
    if (!(a <= b))
    {
        return a - b;
    }

    if (a == c)
    {
        return 10;
    }
    return b - a;
}

class mytest
{
public:
    mytest(uint16_t id, string info) : _id(id), _info(info) {}

    uint16_t get_id() const
    {
        return _id;
    }

private:
    uint16_t _id;
    string _info;
};

class mytestint
{
public:
    mytestint() = default;

    mytestint(uint16_t id, int pr) : _id(id), _pr(pr)
    {
        if (pr < 0)
        {
            throw std::runtime_error("pr < 0 ");
        }
    }

    mytestint(int pr) : _id(0), _pr(pr)
    {
        if (pr < 0)
        {
            throw std::runtime_error("pr < 0 ");
        }
    }

    mytestint(const mytestint &t)
    {
        printf("mytestint(const mytestint &t)\n");
        // if(_id == t._id){
        //     throw std::runtime_error("bad copy");
        // }
    };

    mytestint &operator=(const mytestint &t) = default;

    uint16_t get_id() const
    {
        return _id;
    }

    mytestint &set_id(uint16_t id)
    {
        _id = id;
        return *this;
    }

    mytestint &set_ptr(const std::shared_ptr<int> &ptr)
    {
        _ptr = ptr;
        return *this;
    }

    mytestint &set_str(const std::string &info)
    {
        _info = info;
        return *this;
    }

private:
    uint16_t _id;
    int _pr;
    std::shared_ptr<int> _ptr {nullptr};
    std::string _info;
};

struct compareFunctor
{
    bool operator()(const mytest &a, const mytest &b) const
    {
        // 实现比较逻辑
        return a.get_id() < b.get_id();
    }
};

struct compareFunctorInt
{
    bool operator()(const mytestint &a, const mytestint &b) const
    {
        // 实现比较逻辑
        return a.get_id() < b.get_id();
    }
};

map<uint16_t, mytestint> _test_map;
map<uint16_t, uint16_t> _test_map_pod;
vector<mytestint> _test_vector;

// void test_emplace(uint16_t id, uint16_t pr, const std::string &info)
void test_emplace(uint16_t id, int pr)
{
    printf("%u | %d\n", id, pr);
    // map with class
    mytestint temp = {id, id};
    auto t = _test_map.emplace(id, temp);
    auto kk = _test_map.emplace(100000, pr);  // 尝试调用单参构造函数
    // auto k = _test_map.emplace(id, mytestint(id, pr));

    auto s = _test_map.insert({pr, temp});
    _test_map[id] = mytestint(id, 2);
    auto h = _test_map[pr];
    // map with uint16_t
    _test_map_pod.emplace(id, pr);
    _test_map_pod.insert({pr, id});
    _test_map_pod[id] = pr;

    if (_test_map_pod.empty())
    {
        ;
    }
}

void test_vector_emplace(uint16_t id, int pr)
{
    mytestint temp = {id, id};
    _test_vector.push_back(temp);
    // _test_vector.emplace_back(id, pr);
    _test_vector.emplace_back(pr);

    temp.set_id(13265);
    auto int_temp = std::make_shared<int>(3);
    temp.set_ptr(int_temp);
    // string test_info = "asdt";
    // temp.set_str(test_info);
    // temp.set_str("asdfalkewqlr");

    mytestint test_set;
    test_set.set_id(1).set_ptr(int_temp).set_str("test");
}

void defualt_value_func()
{
    int c = 10 + 10;
}

void defualt_value_func(int a)
{
    int c = a + 10;
}

bool is_flag = true;

void test_default_value1(int a)
{
    is_flag ? defualt_value_func(a) : void();
}

void test_default_value2()
{
    is_flag ? defualt_value_func() : void();
}

void test_exception(bool is_exp)
{
    try
    {
        {
            if (is_exp)
                throw std::runtime_error("test");
        }
    } catch (...)
    {}
}

int *test_new(int sz)
{
    return new int(sz);
}

int *test_new_array(uint64_t sz)
{
    auto temp = new (std::nothrow) int[sz];
    assert(temp != nullptr);
    return temp;
}

template <typename T>
static T *SafeMallocObject()
{
    T *ptr = nullptr;
    while (!ptr)
    {
        ptr = new (std::nothrow) T();
    }
    return ptr;
}

using MY_LOCK_GUARD = std::lock_guard<std::mutex>;

mutex gmt;

bool test_mutex_lock()
{
    // _mt.lock();
    // // do something
    // printf("test mutex\n");
    // _mt.unlock();
    if (gmt.try_lock())
    {
        printf("do something\n");
        gmt.unlock();
        return true;
    }
    return false;
}

void test_mutex_lockguard()
{
    static mutex _mt;
    {
        MY_LOCK_GUARD test(_mt);
        // do something
        printf("test using mutex guard\n");
    }

    {
        std::lock_guard<std::mutex> test_lock(_mt);
        printf("test mutex guard\n");
    }
}

/**
 * @brief 测试参数中的string插入会不会有问题
 *
 * @param    recv_msg            Description
 */
uint64_t test_string_assign(char *recv_buf, uint64_t recv_bytes)
{
    static string recv_msg;

    // 判断是否需要加入进去
    recv_bytes > 0 ? static_cast<void>(recv_msg.assign(recv_buf, recv_bytes)) : void();

    return recv_bytes;
}

// void test_string_plus(const string &local, const string &remote)
// {
//     static string recv_msg;
//     string target("/");
//     if (local < remote)
//     {
//         target += local + "_" + remote;
//     } else
//     {
//         target += remote + "_" + local;
//     }
//     static_cast<void>(recv_msg.assign(target));
//     // 三目运算
//     static_cast<void>(recv_msg.assign("/" + (local < remote ? local + "_" + remote : remote + "_" + local)));

//     char sun_path[128];
//     static_cast<void>(strncpy(sun_path, (recv_msg).c_str(), sizeof(sun_path) - 1));
// }

/**
 * @brief 测试str相加和追加操作造成的分支
 *
 */
void test_string_assign()
{
    string temp = "info";
    temp += " test1";
    temp.assign(" test2", 5);
    temp.push_back('3');
    temp.append(" test4");
    temp.append(" test5", 5);
    string test6str = " test6";
    string test7str = " test7";
    temp.append(test6str);
    temp += test6str;

    // 重新相加
    temp = test6str + "_" + test7str;
}

map<int, vector<int>> _test_map_v;

void test_auto_for(int id)
{
    for (auto &v : _test_map_v.at(id))
    {
        cout << v << " ";
    }
    cout << endl;
}


#include <string>
#include <cstring>

void test_string_plus_inl(const std::string &local, const std::string &remote);

// This is just a lcov test function, it does not have actual functionality
void test_string_plus(const std::string &local, const std::string &remote)
{
    static std::string recv_msg;
    std::string target("/");
    if (local < remote)
    {
        target += local + "_" + remote;
    } else
    {
        target += remote + "_" + local;
    }

    char sun_path[128];
    static_cast<void>(strncpy(sun_path, (recv_msg).c_str(), sizeof(sun_path) - 1));
}

#include "main.inl"