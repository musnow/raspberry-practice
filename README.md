# Raspberry-Practice

### 仓库简介
折腾树莓派4b开发板的代码

现在开始用腾讯云CentOS开始系统的学习Linux指令，相关Linux测试使用的代码也会放在本仓库中，不再另外开辟新仓库

### 仓库内容
* 部分代码是在win平台下用VS写了之后传过去的
* 用树莓派来练习git指令，而不使用git可视化工具。避免实习后不了解git指令。
* exp文件夹内为树莓派基础实验的学习，包括了`wiringPi`库的函数知识
* vim文件夹内为使用vim编辑器学习linux操作的代码，尝试摆脱图形化桌面，只用命令行进行操作


后续考虑学习**使用树莓派搭建个人博客、个人nas等操作**的流程

>CSDN博客专栏“没事就吃树莓派”，记录学习过程👉[点我](https://blog.csdn.net/muxuen/category_11654232.html)

### Git指令学习
学习git指令的博客👉[点我](https://blog.csdn.net/chentuo2000/article/details/104834522)

```c
cd home/pi/xxx //打开文件路径
ls //显示当前路径下的文件

git pull//从远程仓库拉取代码
git status//当前本地仓库状态，有没有发生更改

git add xxx //添加xxx文件夹中需要上传的文件到缓存区
git commit -m "xxxxx"//此部分会和仓库push时的注释同步
git push//推送到远程仓库

git reset --soft //撤回git commit –m
git reset --mix //撤回git commit –m和git add
git reset --hard //回退git commit –m、git add和工作区
git reset --hard HEAD~X //将本地commit回退X个版本
//这个操作会删除本地文件的已有更改，建议备份当前文件后再进行操作
git status -sb //清空缓存区
```
仓库状态码`git status -s `解析

```c
git status -s//更详细地显示文件状态

A //表示本地新增的文件（远程仓库没有）
C //文件的一个新拷贝
D //本地删除的文件（远程仓库还有）
M //表示文件有发生更改
R //文件被重命名
T //文件类型被修改
U //文件没有被合并（这部分不是了解）
```

22-06-16：看到了这篇关于git上传信息填写规范的文章[【链接】](https://www.jianshu.com/p/2ab3a991d1f9)，我感觉有必要学习一下，并在之后的git提交中使用相同的格式，方便自己查看，和其他用户查看。

> 提交格式：`type(scope):message`

### vim编辑器

安装vim
```
sudo apt-get install vim
```

需要注意的是，vim编辑器下不能使用`CTRL+S`来保存文件，因为在linux中这个快捷键的作用是暂停该终端，整个系统都会卡住，这时候使用`CTRL+Q`取消暂停就可以了。

以下是命令模式下的一些文本批量化操作
```
yy 复制当前行，nyy复制n行
p  粘贴再当前行的后面，np粘贴n次剪贴板的内容
dd 剪切（删除）当前行，ndd操作n行
u  撤销
ctrl+r  重做
shift+g 光标快速定位到文本末尾
gg 光标快速移动到文本头
n+shift+g 光标定位到文本的第n行
shift+4 光标定位到该行末尾
shift+6 光标定位到该行开头
w,b   以单词为单位进行移动光标
h,j,k,l  左、下、上、右
shift+`  大小写快速切换
r   替换光标所在处的字符，支持nr
shift+r  批量化替换
x  删除光标所在处的字符，nx删除n个
```

vim进入`插入模式`的快捷键有`a i o`，分别对应不同的功能（见博客）

vim编辑器中`底行模式`的一些操作如下
```
:w   "只保存
:q   "不保存退出
:wq  "保存并退出
:reg "打开vim的寄存器面板
:syntax on "开启语法高亮
:set nu    "显示行号
:set nonu  "取消行号显示
:set tabstop=4 "设置tab的缩进，默认为8
:set softtabstop=4 "softtabstop是“逢8空格进1制表符”,前提是你tabstop=8
:set shiftwidth=4 "设置程序自动缩进所使用的空格长度
:set autoindent "自动对齐上一行
:set mouse=a "设置鼠标模式，默认是a
```
上面的一些配置，如果需要写入`vimrc`配置文件，需要先把:和注释都去掉

部分内容参考这篇博客👉[传送门](https://blog.csdn.net/langxianwenye/article/details/17223807)

### Linux系统语句学习

```bash
cd home/pi/xxx #打开文件路径
ls        #显示当前路径下的文件
ls -a     #显示当前路径下的文件，包括隐藏文件
ls -l     #每一行只显示一个文件夹
ll        #和ls -l等价
cat xxx #显示xxx文本文件的内容

mkdir NAME       #创建单个文件夹
mkdirs NAME1/NAME2... #创建文件夹NAME1以及它的子文件夹NAME2

mv test.c ./VimTest #移动当前目录下的test.c文件到下一级目录中
mv test.c ../VimTest #移动当前目录下的test.c文件到上一级目录中
mv test.c ~/VimTest #移动test.c文件到初始目录中
#需要注意的是，如果待移动的目录中没有这个文件夹，那就会把test.c移动之后改名为VimTest，而不会自动创建一个VimTest文件夹
#所以mv指令也用于重命名文件

rm NAME1     #删除NAME1文件（注意是文件）
rm -r NAME2  #递归删除NAME2文件夹以及它里面的内容
rm -rf NAME3 #强制删除文件夹NAME3，不提示选项
#r表示是递归；f表示force强制
```
使用`rm -rf /`的时候，请注意你自己在做什么。虽然大家经常拿这个语句开玩笑“从删库到跑路”
但是在正式工作的时候，机会只有一次，删库跑路是有法律责任的

---

又学了一招，如何把某个文件路径下的其他文件移动到一个子文件夹中

~~~bash
mv  `ls |grep -v TestRefer|xargs` ./TestRefer
~~~

如下图，我把所有文件都移动到了`TestRefer`文件夹中

![](./code/vim/mvTEST.png)

#### ps循环监控脚本

我们可以使用一个监控脚本来更方便的监控进程的结果

~~~bash
while :; do ps jax | head -1 && ps jax | grep test | grep -v grep;sleep 1; echo "########################"; done
~~~

上面这个语句的作用是，每一秒执行一次`ps jax | head -1 && ps jax | grep test | grep -v grep`命令，直到我们使用`ctrl+c`终止进程

需要注意分隔符，`while`后面的是`:;`不要写成双冒号！

## 日志记录
* 22.03.02：出现上传后gitee没有统计小绿点的问题，查看官方帮助文档才发现，是邮箱设置有问题。大部分地方的邮箱不分大小写，但是git里面配置的邮箱需要小写，需要和gitee邮箱设置里面的邮箱同步。[关于该问题的官方帮助文档](https://gitee.com/help/articles/4311)

* 22.03.04：安装了树莓派下的markdown编辑器`ReText`和`Remarkable`。前者竟然连`代码块`还有`> 引用的注释`都无法正常渲染出来（可能需要做一些设置），还是第二个软件好用一点。[树莓派安装md编辑器的参考博客](https://zhuanlan.zhihu.com/p/28713438)

* 22.03.14：linux环境下无法使用`windows.h`头文件，需要使用sleep函数时，要引用头文件`unistd.h`，同时需要注意linux环境下的sleep函数的单位是秒，而VS下是毫秒

* 22.04.01：linux环境下gcc编译器使用delay函数也需要引用`unistd.h`头文件，而Geany编译器可以直接编译通过，无需引用该头文件。

* 22.04.06：linux下gcc编译`.h`文件会报错，编译链接的时候无需编译头文件。

* 22.04.10：`softPwmWrite`函数可以遵循标准RGB表格来设置RGBled的颜色。这里有一个关于该函数参数的疑惑，但是一直没有得到解决👉[传送门](https://ask.csdn.net/questions/7679151?weChatOA=weChatOA1)

* 22.04.12：今天尝试安装了vim编辑器，遇到了很多问题，好在现在网上有很多解决方法，最后都顺利搞定了！ [博客记录](https://blog.csdn.net/muxuen/article/details/124123421?spm=1001.2014.3001.5502)

* 22.04.16：之前尝试了好久，都不能把vim里面的东西复制到windows下（比如复制代码到win下方便写博客）现在能确定是vim没有`+和*号`寄存器的问题，换源、修改vimrc配置文件等等操作都试过了，依旧不得行，放弃了。【注：不确定是安装vim的方式有问题，还是树莓派4b的系统和vim不太兼容的问题。到此打住吧，不然需要浪费太多时间弄这个，不值得！与其折腾在vim里面如何复制，不如Xftp直接打开源文件复制。到头来发现最简单的方法反而是最好的】

* 22.05.03：我使用了cpolar实现了内网穿透，并让朋友在远程成功连接了树莓派。这个软件的操作可以让树莓派摇身一变,成为一个远程的Linux服务器。可玩性也就更高了！不过现在都在学校里呆着，暂时还不需要远程操控树莓派。不过可以共享给朋友，让他练习一下Linux指令和编译操作。[博客记录](https://blog.csdn.net/muxuen/article/details/124548332?spm=1001.2014.3001.5501)

* 22.05.06：今天配置好了腾讯云的CentOS7.6的Linux使用环境，和树莓派有一定差距，但是主要内容都是相同的。而且我新学了一个cat指令（显示文本文件的内容）这个指令可以直接解决之前vim编辑器的复制问题。

  但很快新的问题就出现了，vim编辑器内代码并没有乱码，但是cat出来的内容不完整，有部分乱码（其实复制成RTF格式后粘贴到win下是无乱码的）搜了几个教程并没有解决这个问题
 同时我还发现原本设置好的git的SSH密匙，每一次重新登录终端就会被重置，这是为什么？

* 22.05.07：现在使用git账户和密码的`global config`设置解决了这个问题，可以愉快使用了！

* 22.06.20: ssh的问题已经找到，是一个莫须有的配置操作导致的。去掉那个操作反而就是正确的情况了！博客已经更新相关内容！[链接](https://ewait.gitee.io/2022/05/09/note_Linux/1%E4%BD%BF%E7%94%A8ssh%E8%BF%9E%E6%8E%A5git/)
