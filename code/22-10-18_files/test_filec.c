#include<stdio.h>

void writetest()
{
    char*file="test.txt";
    FILE* f=fopen(file,"w");

    for(int i =0;i<10;i++)
    {
        fprintf(f,"hello linux! %d\n",i);
    }
    
    fclose(f);
    f=NULL;
}

void readtest()
{
    char*file="test.txt";
    FILE* f=fopen(file,"r");

    char buff[128];//将读取到的数据写入buff数组里面（会自带\0）
    for(int i =0;i<10;i++)
    {
        fgets(buff,20,f);
        printf("%s",buff);
    }
    fclose(f);
    f=NULL;
}

int main()
{
    writetest();

    readtest();




    return 0;
}