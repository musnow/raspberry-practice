#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#define SIZE 256

void test1()
{
    char buf[SIZE];
    int fd1 = open("test.txt", O_RDONLY);
    read(fd1,buf,strlen(buf)-1);
    ssize_t s = read(fd1, buf, sizeof(buf)-1);
    if(s > 0)
    {
       buf[s] = '\0';
       printf("%s", buf);
    }

    close(fd1);
}

void test2()
{
    umask(0);//先把umask设置为0，保证权限值设置正确，不受系统umask影响
    //fopen("test.txt", "w"); //底层调用open，O_WRONLY | O_CREAT | O_TRUNC
    //fopen("test.txt", "a"); //底层调用open，O_WRONLY | O_CREAT | O_APPEND
    //int fd = open("test.txt", O_WRONLY | O_CREAT | O_APPEND, 0666);

    printf("begin fd1\n");
    int fd1 = open("test.txt", O_WRONLY | O_CREAT, 0666);
    sleep(5);
    printf("begin fd2\n");
    int fd2 = open("test.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    sleep(5);


    printf("fd1: %d  fd2: %d\n",fd1,fd2);

    const char *str = "bbadsfasdfasdfa23123bbbb";
    write(fd1, str, strlen(str));

    close(fd1);
}

void test3()
{
    int fda = open("loga.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    int fdb = open("logb.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    int fdc = open("logc.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    int fdd = open("logd.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    int fde = open("loge.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    printf("fda: %d\n", fda);
    printf("fdb: %d\n", fdb);
    printf("fdc: %d\n", fdc);
    printf("fdd: %d\n", fdd);
    printf("fde: %d\n", fde);
}

void test4()
{
    //c语言中的FILE是一个结构体，里面管理了linux系统的文件描述符
    printf("stdin %d\n",stdin->_fileno);//  0
    printf("stdout %d\n",stdout->_fileno);// 1
    printf("stderr %d\n",stderr->_fileno);// 2

    FILE* f1 = fopen("test.txt","w");
    printf("f1 %d\n",f1->_fileno);//3
}

void test5()
{
    char buf[SIZE]="12345678910\n";
    write(1,buf,strlen(buf));
}

void test6()
{
    char buf[SIZE];
    ssize_t s = read(0,buf,sizeof(buf));
    if(s>0)
    {
        buf[s]='\0';
        printf("stdin: %s\n",buf);
    }

}

void test7()
{
    int fda = open("loga.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    int fdb = open("logb.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);    
    printf("fda: %d\n", fda);
    printf("fdb: %d\n", fdb);
    close(fda);
    printf("\n");

    int fdc = open("logc.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    printf("fdc: %d\n", fdc);
}

void test8()
{
    //文件描述符分配的时候，会在数组里面找第一个为空的描述符
    // printf("start test!\n");
    // close(1);//关闭stdout
    // int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    // printf("fd: %d\n",fd);
    // close(fd);

    //如果在close之前不进行打印，则需要进行fflush才能输出到文件中
    close(1);//关闭stdout
    int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    printf("fd: %d\n",fd);
    fflush(stdout);//刷新缓冲区
    close(fd);
}


//输出重定向
void test9()
{
    int fd = open("log.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666);
    dup2(fd, 1);
    int ret = dup2(fd, 1);//
    if(ret > 0) 
        close(fd);

    printf("ret: %d\n", ret);//ret为newfd
    //本来往显示器打印，最终变成向指定文件打印 -> 重定向
    fprintf(stdout, "打开文件成功，fd: %d\n", fd);
    fflush(stdout);//刷新缓冲区
    close(fd);
}
//追加重定向
void test10()
{
    int fd = open("log.txt", O_WRONLY | O_CREAT | O_APPEND, 0666);
    dup2(fd, 1);
    int ret = dup2(fd, 1);//
    if(ret > 0) 
        close(fd);

    printf("ret: %d\n", ret);//ret为newfd
    //本来往显示器打印，最终变成向指定文件打印 -> 重定向
    fprintf(stdout, "打开文件成功，fd: %d\n", fd);
    fflush(stdout);//刷新缓冲区
    close(fd);
}
//输入重定向
void test11()
{
    int fd = open("log.txt",O_RDONLY);
    if(fd<0)
    {
        perror("open");
        return ;
    }

    int ret = dup2(fd,0);//重定向stdin
    if(ret > 0) 
        close(fd);

    char buf[128];
    while(fgets(buf,sizeof(buf),stdin)!=NULL)
    {
        printf("%s",buf);
    }
}
 
int main()
{
    test11();


    return 0;
}