#include <stdio.h>
#include <string.h>
#include <unistd.h>
//一个简单的进度条
#define NUM 102
#define STYLE '#'

void process()
{
    char bar[NUM];
    memset(bar, '\0', sizeof(bar));

    const char *lable = "|/-\\";//在末尾打印一个转动的“小圆圈”

    int cnt = 0;
    while(cnt <= 100)
    {
        printf("加载中:%-100s[%d%%][%c]\r", bar, cnt, lable[cnt%4]);
        //printf("加载中:\033[46;34m%-100s\033[0m[%d%%][%c]\r", bar, cnt, lable[cnt%4]);//给打印添加颜色
        fflush(stdout);
        bar[cnt++] = STYLE;//打印预定义的符号
        usleep(200000);
    }
    printf("\n");
}

int main()
{
    process();

    return 0;
}
