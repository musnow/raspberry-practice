#include <stdio.h>
#include <string.h>

void test_bzero()
{
    char str[] = "1234567890";
    int sz = strlen(str);
    printf("%s\n", str);
    bzero(str, 3); // 将前三个字节写为0
    for (int i = 0; i < sz; i++)
    {
        printf("%c", str[i]);
    }
    printf("\n");
}

void test_cmp()
{
    char str1[] = "abc";
    char str2[] = "ABC";
    printf("strcmp:     %d\n", strcmp(str1, str2));
    printf("strcasecmp: %d\n", strcasecmp(str1, str2));
}

int main()
{
    test_cmp();
    return 0;
}