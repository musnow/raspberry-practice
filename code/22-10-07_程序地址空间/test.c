#include<stdio.h>
#include<stdlib.h>

int un_global_val;//未初始化全局变量
int global_val=100;//已初始化全局变量
//main函数的参数
int main(int argc, char *argv[], char *env[])
{
    printf("code addr         : %p\n", main);
    printf("init global addr  : %p\n", &global_val);
    printf("uninit global addr: %p\n", &un_global_val);
    char *m1 = (char*)malloc(100);
    char *m2 = (char*)malloc(100);
    char *m3 = (char*)malloc(100);
    char *m4 = (char*)malloc(100);
    int a = 100;
    static int s = 100;
    printf("heap addr         : %p\n", m1);
    printf("heap addr         : %p\n", m2);
    printf("heap addr         : %p\n", m3);
    printf("heap addr         : %p\n", m4);

    printf("stack addr        : %p\n", &m1);
    printf("stack addr        : %p\n", &m2);
    printf("stack addr        : %p\n", &m3);
    printf("stack addr        : %p\n", &m4);
    printf("stack addr a      : %p\n", &a);
    printf("stack addr s      : %p\n", &s);
    printf("\n");
    for(int i = 0; i < argc; i++)
    {
        printf("argv addr         : %p\n", argv[i]);
    }
    printf("\n");
    for(int i =0 ; env[i];i++)
    {
        printf("env addr          : %p\n", env[i]);
    }
    return 0;
}