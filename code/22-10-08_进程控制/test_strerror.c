#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>

void test1()
{
    int i=0;
    for(i=0;i<200;i++)
    {
        printf("[%d] %s\n",i,strerror(i));
    }
    return;
}

void test2()
{
    printf("i'm good, exit");
    exit(0);
}

void test3()
{
    printf("i'm good, _exit");
    _exit(0);
}

int main()
{
    test3();

    return 0;
}