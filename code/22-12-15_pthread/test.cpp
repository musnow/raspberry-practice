#include<iostream>
#include<string.h>
#include<pthread.h>
#include<thread>
#include<unistd.h>
#include<sys/types.h>
#include<sys/syscall.h>
using namespace std;

int global1 = 10;//全局变量
__thread int global = 100;//可以让线程独立使用的全局变量

void* func1(void* arg)
{
    int a = 5;
    while(a--)
    {
        cout << "func1 thread:: " << (char*)arg << " :: " << getpid() << endl;
        sleep(1);
    }
    cout << "func1 exit" << endl;
    return (void*)100;
}

void* func2(void* arg)
{
    int a = 10;
    while(a--)
    {
        //int b =  a/0;
        cout << "func2 thread:: " << (char*)arg << " :: " << getpid() << " tid: " << syscall(SYS_gettid) << endl;
        sleep(1);
    }
    cout << "func2 exit" << endl;
    //return (void*)10;
    pthread_exit((void*)10);
}

void* func3(void* arg)
{
    //pthread_detach(pthread_self());//自己分离自己
    int a = 7;
    while(a--)
    {
        //cout << "func thread " << (char*)arg <<  " - global: " << global << " - &global: " << &global << endl;
        printf("func thread:%s - global:%d - &global:%p\n",(char*)arg,global,&global);
        global++;
        sleep(1);
    }
    cout << "func exit" << endl;
    return (void*)10;
}

void* func4(void* arg)
{
    int a = 7;
    while(a--)
    {
        printf("func thread:%s - pid:%d - tid:%d\n",(char*)arg,getpid(),syscall(SYS_gettid));
        global++;
        sleep(1);
    }
    cout << "func exit" << endl;
    return (void*)10;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

void test1()
{
    pthread_t t1,t2;

    pthread_create(&t1,nullptr,func2,(void*)"1");
    pthread_create(&t2,nullptr,func2,(void*)"2");

    // cout << "pthread_create "<< t1 << " " << t2 << endl;
    // sleep(2);
   
    while(1)
    {
        printf("tis is main - pid:%d - tid:%d\n",getpid(),syscall(SYS_gettid));
        sleep(1);
    }

    pthread_join(t1,nullptr);
    pthread_join(t2,nullptr);
}

void test2()
{
    thread t1(func1,(char*)"test1");
    thread t2(func2,(char*)"test2");
   
    while(1)
    {
        cout << "this is main:: " << getpid()<<endl;
        sleep(1);
    }

    t1.join();
    t2.join();
}

void test3()
{
    pthread_t t1,t2;

    pthread_create(&t1,nullptr,func1,(void*)"1");
    pthread_create(&t2,nullptr,func2,(void*)"2");



    int a = 15;
    while(a--)
    {
        cout << "this is main:: " << getpid()<<endl;
        sleep(1);
        if(a==11){
            pthread_cancel(t1);
            pthread_cancel(t2);
            break;
        }
    }

    // sleep(2);
    // cout << "pthread_create "<< t1 << " " << t2 << endl;
    //printf("0x%x  0x%x\n",t1,t2);
    
    void* r1;
    void* r2;
    pthread_join(t1,&r1);
    pthread_join(t2,&r2);

    sleep(2);
    cout << "retval 1 : " << (long long)r1 << endl;
    cout << "retval 2 : " << (long long)r2 << endl;

}

void test4()
{
    pthread_t t1,t2;

    pthread_create(&t1,nullptr,func3,(void*)"1");
    pthread_create(&t2,nullptr,func3,(void*)"2");

    // int a = 10;
    // while(a--)
    // {
    //     cout << "this is main - global: " << global << " - &global: " << &global << endl;
    //     sleep(1);
    // }

    sleep(2);

    pthread_detach(t1);
    pthread_detach(t2);

    sleep(1);

    void* r1=nullptr;
    void* r2=nullptr;
    int ret = pthread_join(t1,&r1);
    cout << ret << ":" << strerror(ret) << endl;
    ret = pthread_join(t2,&r2);
    cout << ret << ":" << strerror(ret) << endl;

    cout << "retval 1 : " << (long long)r1 << endl;
    cout << "retval 2 : " << (long long)r2 << endl;

    sleep(20);
}


void test5()
{
    pthread_t t1,t2;

    pthread_create(&t1,nullptr,func2,(void*)"1");
    pthread_create(&t2,nullptr,func2,(void*)"2");

    sleep(1);

    pthread_detach(t1);
    pthread_detach(t2);

    sleep(1);
}

int main()
{    
    test5();
    cout << "main exit" << endl;
    pthread_exit(0);

    return 0;
}