#include<iostream>
#include<string.h>
#include<signal.h>
#include<pthread.h>
#include<thread>
#include<unistd.h>
#include<sys/types.h>
#include<sys/syscall.h>
using namespace std;

pthread_mutex_t m1;//锁1
pthread_mutex_t m2;//锁2

void* func1(void*arg)
{
    while(1)
    {
        pthread_mutex_lock(&m1);
        usleep(200);
        pthread_mutex_lock(&m2);

        cout << "func1 is running... " <<(const char*)arg<<endl;

        pthread_mutex_unlock(&m1);
        pthread_mutex_unlock(&m2);
    }
    //cout << "func1 exit " <<(const char*)arg<<endl;
}
void* func2(void*arg)
{
    while(1)
    {
        pthread_mutex_lock(&m2);
        usleep(300);
        pthread_mutex_lock(&m1);

        cout << "func2 is running... " <<(const char*)arg<<endl;

        pthread_mutex_unlock(&m1);
        pthread_mutex_unlock(&m2);
    }
    //cout << "func2 exit " <<(const char*)arg<<endl;
}

int main()
{
    pthread_mutex_init(&m1,nullptr);
    pthread_mutex_init(&m2,nullptr);

    pthread_t t1,t2;
    pthread_create(&t1,nullptr,func1,(void*)"t1");
    pthread_create(&t2,nullptr,func2,(void*)"t2");

    //分离
    pthread_detach(t1);
    pthread_detach(t2);

    while(1)
    {
        cout << "main running..." <<endl;
        sleep(1);
    }

    pthread_mutex_destroy(&m1);
    pthread_mutex_destroy(&m2);
    return 0;
}