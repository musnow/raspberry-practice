#pragma once 

#include<iostream>
#include<pthread.h>
#include<semaphore.h>
#include<vector>
#include<stdlib.h>
#include<unistd.h>
using namespace std;

template<class T>
class RingQueue
{
private:
    vector<T> _rq;//队列
    sem_t _spaceSem;//空间信号量
    sem_t _valueSem;//数据信号量
    pthread_mutex_t _proMutex;//生产者的锁
    pthread_mutex_t _conMutex;//消费者的锁

    size_t _rear;//尾指针
    size_t _front;//头指针
public:
    RingQueue(int capa = 5)
        :_rq(capa),
        _rear(0),
        _front(0)
    {
        sem_init(&_spaceSem,0,capa);//空间
        sem_init(&_valueSem,0,0);//数量为0

        pthread_mutex_init(&_proMutex,nullptr);
        pthread_mutex_init(&_conMutex,nullptr);
    }
    ~RingQueue()
    {
        sem_destroy(&_spaceSem);
        sem_destroy(&_valueSem);

        pthread_mutex_destroy(&_proMutex);
        pthread_mutex_destroy(&_conMutex);
    }
    //生产者
    void push(T& in)
    {
        sem_wait(&_spaceSem);//获取空间
        pthread_mutex_lock(&_proMutex);

        _rq[_rear] = in; //生产
        _rear++;         // 写入位置后移
        _rear %= _rq.size(); // 更新下标，避免越界

        pthread_mutex_unlock(&_proMutex);
        sem_post(&_valueSem);//释放数量
    }
    //消费
    T pop()
    {
        sem_wait(&_valueSem);
        pthread_mutex_lock(&_conMutex);

        T tmp = _rq[_front];
        _front++;
        _front %= _rq.size();// 更新下标，保证环形特征

        pthread_mutex_unlock(&_conMutex);
        sem_post(&_spaceSem);

        return tmp;
    }

};