#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <cassert>

using namespace std;

void FreeChld(int signo)
{
    assert(signo == SIGCHLD);
    while (true)
    {
        //如果没有子进程了，waitpid就会调用失败
        pid_t id = waitpid(-1, nullptr, WNOHANG); // 非阻塞等待
        if (id > 0)
        {
            cout << "父进程等待成功, child pid: " << id << endl;
        }
        else if(id == 0)
        {
            //还有子进程没有退出
            cout << "尚有未退出的子进程，父进程继续运行" << endl;
            break;
        }
        else
        {
            cout << "父进程等待所有子进程结束" << endl;
            break;
        }
    }
}

int main()
{
    signal(SIGCHLD, FreeChld);
    // 子进程退出的时候，默认的信号处理就是忽略吗？
    // 调用signal/sigaction SIG_IGN, 意义在哪里呢？
    // SIG_IGN手动设置，让子进程退出，不要给父进程发送信号了，并且自动释放
    //signal(SIGCHLD, SIG_IGN);
    for (int i = 0; i < 5; i++)
    {
        pid_t id = fork();
        if (id == 0)
        {
            //子进程
            int cnt = 8;
            while (cnt)
            {
                cout << "子进程 pid: " << getpid() << " cnt: " << cnt-- << endl;
                sleep(1);
            }
            cout << "子进程退出，进入僵尸状态: " << i << endl;
            exit(0);
        }
        sleep(2);
    }

    while (true)
    {
        cout << "父进程正在运行: " << getpid() << endl;
        sleep(1);
    }

    // //如果不用信号捕捉，父进程main中要主动等待
    // if(waitpid(id, nullptr, 0) > 0)
    // {
    //     cout << "父进程等待子进程成功" << endl;
    // }
    return 0;
}