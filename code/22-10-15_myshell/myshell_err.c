//这是有错误的写法，没办法正确分离出参数
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/types.h>

#define SEP " "
#define NUM 1024
#define SIZE 256

char cmd_line[NUM];//命令行输入
char*cmd_args[SIZE];//分离参数

//用一个buffer来存放环境变量
char env_bufferp[SIZE][NUM];
size_t env_num = 0;//环境变量的数量
//系统的环境变量
extern char**environ;

//对应上层的内建命令
//不使用内建命令，则不会生效
int ChangeDir(const char * new_path)
{
    chdir(new_path);
    return 0; // 调用成功
}
//这里导入环境变量之后，不会影响linux的shell
//而是从我们的myshell开始所有子进程都会继承
void PutEnv(char * new_env)
{
    putenv(new_env);
}

int main()
{
    //shell一直死循环
    while(1)
    {
        // 1.显示命令行中的提示符
        printf("[慕雪@FS-1041 当前路径]# ");
        fflush(stdout);//刷新缓冲区，达到不换行的效果

        // 2.获取用户的输入内容
        memset(cmd_line, '\0', sizeof(cmd_line)*sizeof(char));
        fgets(cmd_line, NUM, stdin); //标准输入stdin获取键盘输入内容
        cmd_line[strlen(cmd_line) - 1] = '\0';// 去掉\n回车
        printf("%s\n",cmd_line);//打印内容作为测试

        // 3.分离出命令和参数
        int arg_num=0;
        char*p=cmd_line,*cmd_temp=cmd_line;
        p = strstr(cmd_temp," ");//查找空格
        do{
            int index = p-cmd_temp;//下标
            printf("index: %d\n",index);

            cmd_temp[index]='\0';//设置\0来拷贝
            char args_temp[SIZE];//开空间
            strcpy(args_temp,cmd_temp);
            printf("arg_num: %d\n",arg_num);
            cmd_args[arg_num++]=args_temp;
            printf("args: %s\n",cmd_args[arg_num-1]);

            cmd_temp=cmd_temp+index+1;//往后移动
            printf("temp: %s\n",cmd_temp);
            p = strstr(cmd_temp," ");//查找空格
        }while(p!=NULL);
        char args_temp[SIZE];
        strcpy(args_temp,cmd_temp);
        printf("arg_num: %d\n",arg_num);
        cmd_args[arg_num++]=args_temp;
        for(int i =0;i<arg_num;i++){
            printf("[%d]  %s\n",i,cmd_args[i]);
        }

        // 4.支持别名和内建命令

        // 5.创建程序 替换
        pid_t ret_id = fork();
        if(ret_id == 0)
        {
            // 子进程
            execvp(cmd_args[0], cmd_args);// 6. 程序替换
            exit(1); //执行到这里，子进程一定替换失败
        }
        // 父进程
        int status = 0;
        pid_t ret = waitpid(ret_id, &status, 0);
        if(ret > 0)
        {
            printf("等待子进程成功！code: %d, sig: %d\n", WEXITSTATUS(status),WTERMSIG(status));
        }
    }
    return 0;
}


//变量名别名
typedef struct alias_cmd
{
    char _cmd[SIZE];
    char _acmd[SIZE];
}alias;
alias cmd_alias[SIZE];
size_t alias_num = 0;
//设置别名
void set_alias(char*cmd,char* acmd)
{
    strcpy(cmd_alias[alias_num]._cmd,cmd);
    strcpy(cmd_alias[alias_num]._acmd,acmd);
    alias_num++;
}
//判断一个命令是否有别名
bool is_alias(char* cmd_args[],int sz)
{
    int i=0;
    for(i=0;i<alias_num;i++)
    {
        if(strcmp(cmd_alias[i]._cmd,cmd_args[0])==0)//是别名
        {
            size_t index = 1,j;
            char* cmd_args_temp[SIZE];//创建一个新的数组
            //先把别名中的命令分开
            cmd_args_temp[0] = strtok(cmd_alias[i]._acmd, SEP);
            if(strcmp(cmd_args_temp[0], "ls") == 0 && strcmp(cmd_args[0], "ls") != 0) //别名的时候也需要设置ls的颜色
                cmd_args_temp[index++] = (char*)"--color=auto";
            while(cmd_args_temp[index++] = strtok(NULL, SEP));
            //从原本数组的第二位开始往后设置
            for(j=1;j<alias_num;j++)
            {
                strcpy(cmd_args_temp[index++],cmd_args[j]);
            }
            for(int k =0;k<index;k++){
                printf("temp[%d]  %s\n",k,cmd_args_temp[k]);
            }
            alias_num=index;//替换掉原本的数组
            printf("%d\n",alias_num);
            memcpy(cmd_args,cmd_args_temp,sizeof(cmd_args));
            for(j=0;j<index-1;j++)
            {
                printf("temp[%d] %s\n",j,cmd_args_temp[j]);
                //strcpy(cmd_args[j],cmd_args_temp[j]);//原本的位置没有那么大空间，放不下！
                

                
                printf("args[%d] %s\n",j,cmd_args[j]);
            }
            cmd_args[j]=NULL;
            return true;
        }
    }
    return false;
}
//下面这个程序可以正常执行，但是不够完美。可以直接用strcok替代我们自己写的代码
int test()
{
    while(1)
    {
        // 1.显示命令行中的提示符
        printf("[慕雪@FS-1041 当前路径]# ");
        fflush(stdout);//刷新缓冲区，达到不换行的效果

        // 2.获取用户的输入内容
        memset(cmd_line, '\0', sizeof(cmd_line)*sizeof(char));
        fgets(cmd_line, NUM, stdin); //标准输入stdin获取键盘输入内容
        cmd_line[strlen(cmd_line) - 1] = '\0';// 去掉\n回车
        printf("%s\n",cmd_line);//打印内容作为测试

        // 3.分离出命令和参数
        int arg_num=0;
        char*p=cmd_line,*cmd_temp=cmd_line;
        p = strstr(cmd_temp," ");//查找空格
        while(p!=NULL)
        {
            int index = p-cmd_temp;//下标
            //printf("index: %d\n",index);

            //这里不需要额外的拷贝操作，因为cmd_line的数据不会在当前循环内被清空
            cmd_temp[index]='\0';//设置\0来赋值
            cmd_args[arg_num++]=cmd_temp;
            if(strcmp(cmd_temp,"ls")==0)//ls默认为带颜色
                strcat(cmd_args[arg_num++],(char*)"--color=auto");
            //printf("args: %s\n",cmd_args[arg_num-1]);

            cmd_temp=cmd_temp+index+1;//往后移动
            //printf("temp: %s\n",cmd_temp);
            p = strstr(cmd_temp," ");//查找空格
        }
        cmd_args[arg_num++]=cmd_temp;
        cmd_args[arg_num]=NULL;
        for(int i =0;i<arg_num;i++){
            printf("[%d]  %s\n",i,cmd_args[i]);
        }

        // 4.支持别名和内建命令
        // ls的别名

        // 5.创建程序 替换
        pid_t ret_id = fork();
        if(ret_id == 0)
        {
            // 子进程
            execvp(cmd_args[0], cmd_args);// 6. 程序替换
            exit(1); //执行到这里，子进程一定替换失败
        }
        // 父进程
        int status = 0;
        pid_t ret = waitpid(ret_id, &status, 0);
        if(ret > 0)
        {
            printf("等待子进程成功！code: %d, sig: %d\n", WEXITSTATUS(status),WTERMSIG(status));
        }
    }
    return 0;
}