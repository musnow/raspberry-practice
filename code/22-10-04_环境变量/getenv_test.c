#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>

//main函数可以带参数
//第一个参数指代命令个数，执行该可执行文件时传入的几个命令
//第二个参数是一个指针数组，存放了每一个命令的常量字符串
//第三个参数用于导入环境变量！
int main(int arg,char* argv[],char *envs[])
{
    char* user = getenv("USER");
    if(strcasecmp(user,"muxue")!=0)//strcasecmp忽略大小写
    {
        printf("权限禁止！\n");
        return -1;
    }
    printf("成功执行！\n");


    return 0;
}