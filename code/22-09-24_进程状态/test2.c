#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
int main()
{
    int ret = fork();
    if(ret == 0)
    {
        while(1)
        {
            printf("我是子进程，我的ppid是%d,pid是%d\n",getppid(),getpid());
            sleep(1);
        }
    }
    else{
        int i  = 10;
        while(i--)
        {
            printf("我是父进程，我的ppid是%d,pid是%d\n",getppid(),getpid());
            sleep(1);   
        }
        printf("父进程退出\n");
        exit(0);
    }       
    return 0;
}
