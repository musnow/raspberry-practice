#include "utils.h"
#include "log.hpp"
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

#define CRLF "\r\n"
#define CRLF_LEN strlen(CRLF)
#define SPACE " "
#define SPACE_LEN strlen(SPACE)

#define HOME_PAGE "index.html" // 首页文件
#define ROOT_PATH "web" // 网址根目录地址
const string imageType[] = {"png","jpg","gif","jpeg","pdf","ico"}; // 图片类型(或者说需要用全字节打开的类型)


// 设置302重定向
string set302response(const string& url)
{
    // 尝试进行302重定向
    string response = "HTTP/1.1 302 Temporarily moved\r\n";
    // 要在报头中添加重定向的目的地，字段为Location
    response+= "Location: ";
    response+= url;
    response+= "\r\n\r\n";//连加两个，包括空行
    return response;
}
// 获取文件的后缀
string getFileType(const string& path)
{
    size_t i = path.rfind('.');
    if(i!=string::npos)//找到了
    {
        string filetype(path,i+1);//获取出文件类型
        logging(DEBUG,"Path: %s | fileType: %s",path.c_str(),filetype.c_str()); // 打印用作debug
        return filetype;
    }
    return ""; //没有后缀
}

// 判断请求头中文件类型是否为图片
bool isImg(const string& fileType)
{
    for(auto& t: imageType)
    {
        // 如果完全一致
        if(fileType == t)
        {
            return true;
        }
    }
    return false;
}

// 获取http请求中的路径
string getPath(string http_request)
{
    size_t pos = http_request.find(CRLF);//找到第一行的分隔符
    if(pos == string::npos) 
        return "";
    string request_line = http_request.substr(0, pos);//取出第一行
    //请求的第一行：GET /a/b/c http/1.0
    size_t first = request_line.find(SPACE);// 找到第一个空格
    if(pos == string::npos) 
        return "";
    size_t second = request_line.rfind(SPACE); // 从后往前找空格
    if(pos == string::npos) 
        return "";
    // 找到两个空格了，两个空格之间的就是请求的路径
    string path = request_line.substr(first+SPACE_LEN, second - (first+SPACE_LEN));
    // 对path进行判断，如果path是以/结尾的，则在path中追加index.html文件名
    if(path[path.size()-1] == '/') {
        path += HOME_PAGE; //加上被隐藏的index.html文件名
    }
    return path;
}

// 读取图片文件
string readImgFile(const string& file_path)
{
    ifstream file(file_path, ios::binary);
    // 打开失败，503
    if (!file.is_open()) {
        return "503";
    }

    ostringstream ss;
    ss << file.rdbuf();
    string content = ss.str();
    file.close();

    return content;
}
// 读取txt文件
string readTxtFile(const string& file_path)
{
    // 如果是文件存在但是打开失败了，应该返回50x代表服务器处理错误
    // tcp是面向字节流的，文件需要用二进制打开
    ifstream in(file_path, ios::binary);
    if(!in.is_open()) //文件打开失败
    {
        return "503";//文件打开失败
    }

    // 内容
    string content;
    string line;
    while(getline(in, line))
    {
        content += line;
    }
    in.close();
    return content;
}


// 读取文件
// file_path - 文件路径
// is_img - 是否为图片
string readFile(const string &file_path,bool is_img = false)
{
    //其实这里应该分两种情况，一种是文件不存在，一种是文件打开失败了
    //如果是文件不存在，应该返回404
    if(access(file_path.c_str(),0)!=0)//判断文件是否存在，存在返回0
    {//windows下相同作用的接口为_access，头文件io.h
        return "404";//文件不存在
    }
    // 读取对应的文件
    if(!is_img){
        return readTxtFile(file_path);
    }
    else{
        return readImgFile(file_path);
    }
}

void handlerHttpRequest(int sock,const map<string,string>& fileTypeMap)
{
    cout <<     "########### header-start ##########" << endl;//打印一个分隔线
    char buffer[BUFFER_SIZE];// buffer流
    ssize_t s = read(sock, buffer, sizeof(buffer));
    if(s > 0){
        cout << buffer << endl;
        cout << "########### header-end   ##########" << endl;
    }
    fflush(stdout);
    string path = getPath(buffer);
    // 假设用户请求的是 /a/b 路径
    // 那么服务端处理的时候，就需要添加根目录位置和默认的文件名
    // <root>/a/b/index.html
    // 在本次用例中，根目录为 ./web文件夹，所以完整的文件路径应该是
    // ./web/a/b/index.html

    string resources = ROOT_PATH; // 根目录路径
    resources += path; // 文件路径
    logging(DEBUG,"[sockfd: %d] filePath: %s",sock,resources.c_str()); // 打印用作debug

    // 获取文件的后缀
    string fileType = getFileType(path);
    // 打开文件
    string content = readFile(resources,isImg(fileType));

    // 开始响应
    string response = "HTTP/1.0 200 OK\r\n";
    // 如果readFile返回的是404，代表文件路径不存在
    bool isErr = true;// 是否出现了错误
    if(strcmp(content.c_str(),"404")==0)
    {
        response = "HTTP/1.0 404 NOT FOUND\r\n";
        // 打开404文件
        path = "/404.html";
        resources = ROOT_PATH;
        resources += path;
        content = readFile(resources);
    }
    else if(strcmp(content.c_str(),"503")==0)
    {// 503 代表打开文件错误
        response = "HTTP/1.0 503 ERR\r\n";
        // 打开503文件
        path = "/503.html";
        resources = ROOT_PATH;
        resources += path;
        content = readFile(resources);
    }
    else{
        isErr = false; // 没有错误
    }
        
    // 追加正确的文件类型Content-Type
    response += "Content-Type: ";
    string contentType = "text/plain";
    if(isErr){
        fileType = "html";//出现了错误，文件类型重置为html
    }
    auto it = fileTypeMap.find(fileType);
    if(it != fileTypeMap.end()){
        contentType = (*it).second;
    }

    response += contentType;
    response += "\r\n";
    // 追加后续字段
    response += ("Content-Length: " + to_string(content.size()) + "\r\n");
    response += "Set-Cookie: This is my cookie test\r\n";
    response += "Connection: keep-alive\r\n"; // 长链接
    response += "\r\n";
    cout << "######### response header ##########\n" << response << "######### response end ##########\n";
    fflush(stdout); // 刷新缓冲区
    // 追加内容
    response += content;

    // 发送给用户
    send(sock, response.c_str(), response.size(), 0);

    // sleep(20);// 休眠几秒钟作为测试
}
class TcpServer
{
    void initMap()
    {
        _fileTypeMap.insert({"html","text/html"});
        _fileTypeMap.insert({"txt","text/plain"});
        _fileTypeMap.insert({"xml","text/xml"});
        _fileTypeMap.insert({"jpg","image/jpeg"});
        _fileTypeMap.insert({"jpeg","image/jpeg"});
        _fileTypeMap.insert({"png","image/png"});
        _fileTypeMap.insert({"gif","image/gif"});
        _fileTypeMap.insert({"ico","image/x-icon"});
        _fileTypeMap.insert({"json","application/json"});
        _fileTypeMap.insert({"pdf","application/pdf"});
    }
public:
    TcpServer(uint16_t port,const string& ip="")
     :_port(port), _ip(ip), _listenSock(-1)
    {
        initMap();//0.先初始化map

        // 1.创建socket套接字,采用字节流（即tcp）
        _listenSock = socket(AF_INET, SOCK_STREAM, 0); //本质是打开了一个文件
        if (_listenSock < 0)
        {
            logging(FATAL, "socket:%s:%d", strerror(errno), _listenSock);
            exit(1);
        }
        logging(DEBUG, "socket create success: %d", _listenSock);
        // 1.1 允许端口被复用
        int optval = 1;
        setsockopt(_listenSock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

        // 2. 绑定网络信息，指明ip+port
        // 2.1 先填充基本信息到 struct sockaddr_in
        struct sockaddr_in local;
        memset(&local,0,sizeof(local));//初始化
        // 协议家族，设置为ipv4
        local.sin_family = AF_INET; 
        // 端口，需要进行 本地->网络转换
        local.sin_port = htons(_port);
        // 配置ip
        // 如果初始化时候的ip为空，则调用INADDR_ANY代表任意ip。否则对传入的ip进行转换后赋值
        local.sin_addr.s_addr = _ip.empty() ? htonl(INADDR_ANY) : inet_addr(_ip.c_str());
        // 2.2 绑定ip端口
        if (bind(_listenSock,(const struct sockaddr *)&local, sizeof(local)) == -1)
        {
            logging(FATAL, "bind: %s:%d", strerror(errno), _listenSock);
            exit(2);
        }
        logging(DEBUG,"socket bind success: %d", _listenSock);
        // 3.监听
        // tcp服务器是需要连接的，连接之前要先监听有没有人来连
        if (listen(_listenSock, 2) < 0)
        {
            logging(FATAL, "listen: %s", strerror(errno));
            exit(LISTEN_ERR);
        }
        logging(DEBUG, "listen: %s, %d", strerror(errno), _listenSock);
    }

    ~TcpServer()
    {// 关闭文件描述符
        close(_listenSock);
    }

    void start()
    {
        //signal(SIGCHLD, FreeChild);
        // while(1)
        // {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            // 获取连接
            int conet = accept(_listenSock,(struct sockaddr*)&peer,&len);
            if(conet<0)
            {
                logging(FATAL, "accept: %s", strerror(errno));
                exit(CONN_ERR);//连接错误
            }
            // 获取连接信息
            string senderIP = inet_ntoa(peer.sin_addr);// 来源ip
            uint16_t senderPort = ntohs(peer.sin_port); // 来源端口
            logging(DEBUG, "accept: %s | %s[%d], socket fd: %d", strerror(errno), senderIP.c_str(), senderPort, conet);

            // 提供服务（孙子进程）-2
            pid_t id = fork();
            if(id == 0)
            {
                close(_listenSock);//因为子进程不需要监听，所以关闭掉监听socket
                //又创建一个子进程，大于0代表是父进程，即创建完子进程后父进程直接退出
                if(fork()>0){
                    exit(0);
                }
                // 父进程推出后，子进程被操作系统接管
                    
                // 孙子进程执行
                logging(DEBUG, "new child process");//打印一个新进程的提示信息，方便观察结果
                while(1)
                    handlerHttpRequest(conet,_fileTypeMap);
                
                exit(0);// 服务结束后，退出，子进程会进入僵尸状态等待父进程回收
            }
            // 爷爷进程
            close(conet); 
            pid_t ret = waitpid(id, nullptr, 0); //此时就可以直接用阻塞式等待了
            assert(ret > 0);//ret如果不大于0，则代表等待发生了错误
            sleep(60);// 给一个进程提供服务，直接休眠
        //}
    }

private:
    // 服务器端口号
    uint16_t _port;
    // 服务器ip地址
    string _ip;
    // 服务器socket fd信息
    int _listenSock;
    // 文件类型和http响应头的对照表
    map<string,string> _fileTypeMap;
};


int main(int argc,char* argv[])
{
    //参数只有两个（端口/ip）所以参数个数应该是2-3
    if(argc!=2 && argc!=3)
    {
        cout << "Usage: " << argv[0] << " port [ip]" << endl;
        return 1;
    }
    

    string ip;
    // 3个参数，有ip
    if(argc==3)
    {
        ip = argv[2];
    }
    TcpServer t(atoi(argv[1]),ip);
    t.start();

    return 0;
}