#include<iostream>
#include<unistd.h>
#include<pthread.h>
using namespace std;


//简单封装pthread库中的mutex
class MyMutex{
public:
    MyMutex()
    {
        pthread_mutex_init(&_mutex,nullptr);
    }

    int lock()
    {
        return pthread_mutex_lock(&_mutex);
    }

    int trylock()
    {
        return pthread_mutex_trylock(&_mutex);
    }

    int unlock()
    {
        return pthread_mutex_unlock(&_mutex);
    }

    ~MyMutex()
    {
        pthread_mutex_destroy(&_mutex);
    }
private:
    pthread_mutex_t _mutex;
};


//制作一个RAII风格的锁，跟随生命周期自动加锁和释放
class Lock
{
public:
    Lock()
    {
        _lock.lock();
    }

    ~Lock()
    {
        _lock.unlock();
    }
private:
    MyMutex _lock;
}