#include <iostream>
#include <vector>
#include <map>
#include <functional>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <cstdlib>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <cassert>
using namespace std;

#define NUM 128

//匿名管道
int TestPipe1();
void TestPipe2();

//提供三个lambda表达式
auto func1 = []() {cout << "this is func1,run by " << getpid() <<endl;};
auto func2 = []() {cout << "this is func2,run by " << getpid() <<endl;};
auto func3 = []() {cout << "this is func3,run by " << getpid() <<endl;};
//通过func存在数组中
vector<function<void()>> func_v;
void LoadFunc()
{
    func_v.push_back(func1);
    func_v.push_back(func2);
    func_v.push_back(func3);
}

//有一种概念叫做”负载均衡”，在多线程/多进程操作中较多使用
//其理念就是每一个进程/线程分到的任务应该是平均的，避免出现某一个进程干的活比别人多的情况
void BalanceDivide(const vector<pair<int,int>>& processMap)
{
    //设置初始化
    srand((size_t)time(nullptr));
    int total = 15;//分配20次任务
    while(total>0)
    {
        sleep(1);
        // 选择一个进程, 选择进程是随机的，没有压着一个进程给任务
        // 较为均匀的将任务给所有的子进程 --- 负载均衡
        int pick = rand() % processMap.size();

        // 选择一个任务
        int task = rand() % func_v.size();

        // 把任务给一个指定的进程
        write(processMap[pick].second, &task, sizeof(task));

        // 打印对应的提示信息
        cout << "父进程指派任务->" << task << " 给进程: " << processMap[pick].first << " 编号: " << pick << endl;
        total--;
    }
    //结束后，写入0代表进程终止
    for(int i=0;i<processMap.size();i++)
    {
        int end = -1;
        write(processMap[i].second, &end, sizeof(end));
        cout << i << " stopping process pid = " << processMap[i].first << endl;
        sleep(1);
    }
    cout << "assign task end!" << endl;
}


//子进程工作，参数为pipefd[0]
void Working(int pfd)
{
    cout << "chlid [" << getpid() << "] start working" << endl;
    while(1)
    {
        int optCode = 0;//读取任务下标
        ssize_t s = read(pfd, &optCode, sizeof(optCode));
        if(s == 0)
        {
            break;//读取了0个字节代表错误
        }
        else if(optCode == -1)
        {
            break;//读取到-1，代表终止
        }   
        assert(s == sizeof(int));//判断是否为正确的size

        // 执行父进程提供的任务
        if(optCode < func_v.size()) 
        {
            func_v[optCode]();
        }
    }
    cout << "chlid [" << getpid() << "] end working" << endl;
}

int main()
{   
    LoadFunc();//加载
    vector<pair<int,int>> assignMap;
    int processNum = 5;
    for(int i=0;i<processNum;i++)
    {
        int pipefd[2];
        if(pipe(pipefd)!=0)
        {
            cerr << "pipe error" << endl;
            continue;
        }

        int pid = fork();
        if(pid==0)//子进程
        {
            close(pipefd[1]);//关闭写
            //开始工作
            Working(pipefd[0]);
            close(pipefd[0]);
            exit(0);//退出子进程
        }

        close(pipefd[0]);//父进程关闭读
        pair<int,int> p = {pid, pipefd[1]};//进程pid和pipefd写端的键值对
        assignMap.push_back(p);
        sleep(1);
    }
    cout << "create all process success!" << endl;
    BalanceDivide(assignMap);//分配任务

    //结束分配后，等待子进程停止运行
    for (int i = 0; i < processNum; i++)
    {
        if (waitpid(assignMap[i].first, nullptr, 0) > 0)
        {
            cout << "wait for pid = " << assignMap[i].first << " wait success! "
                 << "num: " << i << endl;
        }
        close(assignMap[i].second);
    }
    
    return 0;
}


//匿名管道
int TestPipe1()
{
    // 1.创建管道
    int pipefd[2] = {0};
    if(pipe(pipefd) != 0)
    {
        cerr << "pipe error" << endl;
        return 1;
    }
    // 2.创建子进程
    pid_t id = fork();
    if(id < 0)
    {
        cerr << "fork error" << endl;
        return 2;
    }
    else if (id == 0)
    {
        // 3.子进程管道
        // 子进程来进行读取, 子进程就应该关掉写端
        close(pipefd[1]);
        char buffer[NUM];
        while(1)
        {
            cout << "time_stamp: " << (size_t)time(nullptr) << endl;
            // 子进程没有带sleep，为什么子进程你也会休眠呢？？
            memset(buffer, 0, sizeof(buffer));
            //cout << "chlid sleep" << endl;
            //sleep(10);
            ssize_t s = read(pipefd[0], buffer, sizeof(buffer) - 1);
            if(s > 0)
            {
                //读取成功
                buffer[s] = '\0';
                cout << "子进程收到消息，内容是: " << buffer << endl;
            }
            else if(s == 0)
            {
                cout << "父进程写完，子进程退出" << endl;
                break;
            }
            else
            {
                cerr << "err while chlid read pipe" << endl;
            }
            //sleep(1);
        }
        close(pipefd[0]);
        exit(0);
    }
    else
    {
        // 4.父进程管道
        // 父进程来进行写入，就应该关掉读端
        close(pipefd[0]);
        const char *msg = "我是父进程, 这次发送的信息编号是";
        int cnt = 0;
        while(1)
        {
            char sendBuffer[NUM];
            sprintf(sendBuffer, "%s : %d", msg, cnt);//格式化控制字符串
            //cout << "sleep(20)" << endl;
            //sleep(20);
            write(pipefd[1], sendBuffer, strlen(sendBuffer));//写入管道
            cout << "cnt: " << cnt << endl;
            cnt++;
            sleep(1);
        }
        close(pipefd[1]);
        cout << "父进程结束写入" << endl;
    }
    // 父进程等待子进程结束
    pid_t res = waitpid(id, nullptr, 0);
    if(res > 0)
    {
        cout << "等待子进程成功" << endl;
    }

    cout << "父进程退出" <<endl;
    return 0;
}

void TestPipe2()
{
    // 1.创建管道
    int pipefd[2] = {0};
    if(pipe(pipefd) != 0)
    {
        cerr << "pipe error" << endl;
        return ;
    }
    cout << pipefd[0] << " " << pipefd[1] << endl;
    //3  4
}