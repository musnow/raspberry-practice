#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#define SIZE 256

void test1()
{
    printf("test printf ");
    //fflush(stdout);
    const char*msg = "test write ";
    write(stdout->_fileno,msg,strlen(msg));
    
    sleep(3);
}

void test2()
{
    printf("test printf ");
    fprintf(stdout,"test fprintf ");
    fputs("test fputs ",stdout);
    //fflush(stdout);
    const char*msg = "test write ";
    write(stdout->_fileno,msg,strlen(msg));
    
    sleep(3);
    close(stdout->_fileno);
}

void test3()
{
    const char*str1="test printf\n";
    const char*str2="test fprintf\n";
    const char*str3="test fputs\n";
    const char*str4="test write\n";

    //C语言
    printf(str1);//这样写也是ok的
    fprintf(stdout,str2);
    fputs(str3,stdout);

    //系统接口
    write(stdout->_fileno,str4,strlen(str4));
    //子进程创建
    fork();
}

int main()
{
    test3();

    return 0;
}