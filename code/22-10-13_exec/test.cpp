#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    cout << "hello c++" << endl;
    cout << "-------------------------------------------\n";
    cout << "PATH:" << getenv("PATH") << endl;
    cout << "-------------------------------------------\n";
    cout << "MYPATH:" << getenv("MYPATH") << endl;
    cout << "-------------------------------------------\n";
    
    return 0;
}