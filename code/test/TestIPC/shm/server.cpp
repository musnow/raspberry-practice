#include <cstring>

#include "mykey.hpp"

int main()
{
    //获取key值
    key_t key = CreateKey();
    //创建共享内存
    int id = shmget(key, NUM, IPC_CREAT | IPC_EXCL | 0666);
    if (id < 0)
    {
        cerr << "shmget err: " << strerror(errno) << endl;
        return 1;
    }
    cout << "[server] shmget success: " << id << endl;
    // 关联共享内存
    char *str = (char *)shmat(id, nullptr, 0);
    cout << "[server] shmat success\n" << endl;
    // 初始化共享内存的第一个int为-1
    int read_flag = -1;
    memcpy(str, &read_flag, sizeof(read_flag));  // 将前四个字节写-1
    //读取数据
    int recv_data = 0;
    while (true)
    {
        bool ret = ServerWait(str);  //通过共享内存等待
        if (ret)
        {
            memcpy(&recv_data, str + 4, sizeof(recv_data));
            memcpy(str, &read_flag, sizeof(read_flag));  // 将前四个字节写-1
            cout << "[server] recv: " << recv_data << endl;
            if (recv_data == 0)
            {
                break;
            }
        } else
        {
            cout << "[server] waiting for data" << endl;
        }
        sleep(1);
    }

    //去关联
    shmdt(str);  //shmat的返回值
    printf("[server] shmdt(str)\n");
    //删除共享内存
    shmctl(id, IPC_RMID, nullptr);
    printf("[server] exit\n");
    return 0;
}