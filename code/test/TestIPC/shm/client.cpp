#include "mykey.hpp"

int main()
{
    //获取key值
    key_t key = CreateKey();
    //创建共享内存
    int id = shmget(key, NUM, IPC_CREAT);
    if (id < 0)
    {
        cerr << "shmget err: " << strerror(errno) << endl;
        return 1;
    }
    cout << "[client] shmget success: " << id << endl;
    //关联共享内存
    char *str = (char *)shmat(id, nullptr, 0);
    cout << "[client] shmat success\n" << endl;
    sleep(1);
    //读取数据
    int index = 9;  // 从9开始写入
    int write_flag = 0;
    while (index >= 0)
    {
        bool ret = ClientWait(str);
        if (ret)
        {
            memcpy(str + 4, &index, sizeof(index));
            memcpy(str, &write_flag, sizeof(write_flag));  // 将前四个字节写0
            cout << "[client] sent: " << index << endl;
            index--;
        } else
        {
            cout << "[client] waiting" << endl;
        }
        sleep(1);
    }

    //去关联
    shmdt(str);  //shmat的返回值
    printf("[client] shmdt(str)\n");
    //删除共享内存
    shmctl(id, IPC_RMID, nullptr);
    printf("[client] exit\n");
    return 0;
}