#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define BUF_SIZE 1024

int main()
{
    int pipefd[2];
    if (pipe(pipefd) != 0)
    {
        perror("pipe open error!\n");
        return 1;
    }

    int pid1 = fork();
    if (pid1 == 0)  //子进程1
    {
        close(pipefd[0]);  //关闭读
        char buf[BUF_SIZE];
        strcpy(buf, "this is children 1\n");
        write(pipefd[1], buf, strlen(buf));
        close(pipefd[1]);  // 关闭写
        exit(0);           //退出子进程
    }

    int pid2 = fork();
    if (pid2 == 0)  //子进程2
    {
        close(pipefd[0]);  //关闭读
        char buf[BUF_SIZE];
        strcpy(buf, "this is children 2\n");
        write(pipefd[1], buf, strlen(buf));
        close(pipefd[1]);  // 关闭写
        exit(0);           //退出子进程
    }
    // 父进程处理
    sleep(1);  // 等待1s
    close(pipefd[1]);  //父进程关闭写
    char buf[BUF_SIZE];
    if (read(pipefd[0], buf, BUF_SIZE) != -1)
    {
        printf("%s\n", buf);
    }

    printf("parent exit\n");
    return 0;
}