#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// 尝试创建一个子进程
int main()
{
    int pid;
    pid = fork();
    if (pid == 0)
    {
        printf("i am a son process!\n");
        printf("my process id is %d\n", pid);
    } else
    {
        printf("i am the father process!\n");
        printf("my process id is %d\n", pid);
    }
    return 0;
}