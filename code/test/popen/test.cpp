#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <error.h>


char readBuf[1024] = {0};

void test1(FILE* f)
{   
    f = popen("ls","r");
    fread(readBuf,1024,1,f);

    printf("%s\n",readBuf);
    pclose(f);
}

void test2(FILE* f)
{
    f = popen("ls","w");

    // 缓冲区中写入数据
    strcpy(readBuf,"ls -a");
    printf("cmd: %s\n",readBuf);
    // 写道管道文件中
    fwrite(readBuf,1024,1,f);
    int ret = pclose(f);
    printf("pclose: %d | %s\n",ret,strerror(errno));
}



int main()
{
    FILE *f;

    //test1(f);
    test2(f);
    
    //pclose(f);//不能在这里close，会出现段错误
    return 0;
}