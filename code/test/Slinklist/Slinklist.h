﻿#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLTDataType;

typedef struct SListNode
{
	SLTDataType data; // val
	struct SListNode* next; // 存储下一个节点的地址
}SListNode, SLN;

void SListPrint(SListNode* phead);//打印链表
void SListDestroy(SListNode** pphead);//摧毁链表

SListNode* BuySListNode(SLTDataType x);//创建新节点

//因为需要更改节点内部的内容，所以要传入二级指针而不是一级
void SListPushBack(SListNode** pphead, SLTDataType x);//尾插
void SListPushFront(SListNode** pphead, SLTDataType x);//头插

void SListPopBack(SListNode** pphead);//尾删
void SListPopFront(SListNode** pphead);//头删

SListNode* SListFind(SListNode* phead, SLTDataType x);//查找
void SListmodify(SListNode* phead, SListNode* pos, SLTDataType x);//更改

//在pos位置之前插入（pos第几个元素的地址）
//和顺序表的下标越界不同，链表不存在下标，pos地址必须由find函数找到
void SListInsert(SListNode** pphead, SListNode* pos, SLTDataType x);
//在pos位置之后插入
void SListInsertAfter(SListNode** pphead, SListNode* pos, SLTDataType x);


//删除pos位置
//这里应该传入二级指针，因为删除后pos指向的空间已经被free
//所以需要把main函数中的s给置NULL
void SListErase(SListNode** pphead, SListNode** pos);
//删除pos下一位
void SListEraseAfter(SListNode** pphead, SListNode* pos);
