﻿#define _CRT_SECURE_NO_WARNINGS 1

#include "Slinklist.h"


void SListPrint(SListNode* phead)
{
	SListNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

void SListDestroy(SListNode** pphead)
{
	assert(pphead);

	SListNode* curt = *pphead;
	while (curt)
	{
		SListNode* next = curt->next;
		free(curt);
		curt = next;
	}

	*pphead = NULL;

	return ;
}


SListNode* BuySListNode(SLTDataType x)
{
	SListNode* newnode = (SListNode*)malloc(sizeof(SListNode));
	if (newnode == NULL)
	{
		printf("malloc fail\n");
		exit(-1);
	}
	else
	{
		newnode->data = x;
		newnode->next = NULL;
	}

	return newnode;
}

void SListPushBack(SListNode** pphead, SLTDataType x)
{
	assert(pphead);

	SListNode* newnode = BuySListNode(x);
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SListNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}

		tail->next = newnode;
	}
}

void SListPushFront(SListNode** pphead, SLTDataType x)
{
	assert(pphead);

	SListNode* newnode = BuySListNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

void SListPopBack(SListNode** pphead)
{
	assert(pphead);
	
	if (*pphead==NULL)
	{
		return;
	}
	else if((*pphead)->next==NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		//有多个尾巴的情况
		SListNode* tail = *pphead;//找尾巴
		while (tail->next->next != NULL)
		{
			tail = tail->next;
		}
		free(tail->next);
		tail->next = NULL;
	}
	
	return;
}

void SListPopFront(SListNode** pphead)
{
	assert(pphead);
	
	if (*pphead == NULL)
	{
		return;
	}
	else
	{
		SListNode* tail = *pphead;
		*pphead = (*pphead)->next;
		free(tail);
		tail = NULL;
	}

	return;
}

SListNode* SListFind(SListNode* phead, SLTDataType x)
{
	assert(phead);

	SListNode* curt = phead;//找x的位置
	while (curt->next != NULL)
	{
		if (curt->data == x)
		{
			return curt;
		}
		else
		{
			curt = curt->next;
		}

	}
	
	return NULL;
}


void SListmodify(SListNode* phead, SListNode* pos, SLTDataType x)
{
	assert(phead);

	pos->data = x;

	return;
}


void SListInsert(SListNode** pphead, SListNode* pos, SLTDataType x)
{
	assert(pphead);
	assert(pos);
	
	if (pos == *pphead)
	{
		SListPushFront(pphead, x);
	}
	else
	{
		SListNode* tail = *pphead;//找pos前一个
		while (tail->next != NULL)
		{//只写了这个，没有考虑pos是第一个的情况
			if (tail->next == pos)
			{
				tail->next = BuySListNode(x);
				tail->next->next = pos;
				return;
			}
			else
			{
				tail = tail->next;
			}
		}

	}
	
	return;
}

void SListInsertAfter(SListNode** pphead, SListNode* pos, SLTDataType x)
{
	assert(pphead);
	assert(pos);

	SListNode* newnode = BuySListNode(x);
	SListNode* next = pos->next;//原本pos的下一位
	pos->next = newnode;//新插入的pos下一位
	newnode->next = next;

	return;
}


void SListErase(SListNode** pphead, SListNode** pos)
{
	assert(pphead);
	assert(pos);

	if (*pos == *pphead)
	{
		SListPopFront(pphead);
	}
	else
	{
		SListNode* tail = *pphead;//找pos前一个
		while (tail->next != NULL)
		{
			if (tail->next == *pos)
			{
				tail->next = (*pos)->next;
				free(*pos);
				*pos = NULL;
				pos = NULL;
				return;
			}
			else
			{
				tail = tail->next;
			}
		}
	}

	return;
}

void SListEraseAfter(SListNode** pphead, SListNode* pos)
{
	assert(pphead);
	assert(pos);

	SListNode* next = pos->next->next;
	free(pos->next);
	pos->next = next;

	return;
}