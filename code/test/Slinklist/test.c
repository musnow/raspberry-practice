﻿#define _CRT_SECURE_NO_WARNINGS 1

#include "Slinklist.h"


void test1()
{
	SListNode* node1 = (SListNode*)malloc(sizeof(SListNode));
	SListNode* node2 = (SListNode*)malloc(sizeof(SListNode));
	SListNode* node3 = (SListNode*)malloc(sizeof(SListNode));
	SListNode* node4 = (SListNode*)malloc(sizeof(SListNode));
	node1->data = 1;
	node2->data = 2;
	node3->data = 3;
	node4->data = 4;

	node1->next = node2;
	node2->next = node3;
	node3->next = node4;
	node4->next = NULL;
}

void test2(SListNode* slist)
{
	SListNode* s = SListFind(slist, 3);
	if (s != NULL)
	{
		SListmodify(slist,s, 8);
		SListPrint(slist);
		SListInsert(&slist, slist, 7);
		SListPrint(slist);
		SListEraseAfter(&slist, s);
		SListInsertAfter(&slist, s, 8);
		SListPrint(slist);
	}
	else
		printf("err\n");
	return;
}

int main()
{
	
	SListNode* slist = NULL; //空链表
	SListPushBack(&slist, 1);
	SListPushBack(&slist, 2);
	SListPushBack(&slist, 3);
	SListPushBack(&slist, 4);
	SListPrint(slist);

	SListPushBack(&slist,5);
	SListPushFront(&slist, 0);
	SListPrint(slist);

	test2(slist);

	/*SListPopBack(&slist);
	SListPopFront(&slist);
	SListPrint(slist);*/


	return 0;
}


