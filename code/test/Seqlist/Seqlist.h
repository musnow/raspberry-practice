﻿#pragma once

#define CAPA 3 //初始容量

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//#include <Windows.h>


typedef int SLDataType;
//和普通的整型区分开，以此命名的数据和顺序表直接相关

//动态顺序表
typedef struct SeqList
{
	SLDataType* a;
	int size;     // 存储数据个数
	int capacity; // 存储空间大小
}SeqList;

void CheckCapacity(SeqList* sql);

void SQLinst(SeqList* sql);//初始化顺序表
void SQLdestory(SeqList* sql);//拆掉顺序表

void SQLprint(SeqList* sql);//打印

void SQLpushback(SeqList* sql, size_t x);//尾插
void SQLpopback(SeqList* sql);//尾删

void SQLpushfront(SeqList* sql,size_t x);//头插
void SQLpopfront(SeqList* sql);//头删

void SQLinsert(SeqList* sql,size_t pos,size_t x);//在pos位置插入x，pos是下标
void SQLerase(SeqList* sql, size_t pos);//在pos位置删除数据

int SQLfind(SeqList* sql, size_t x);//在顺序表中查找x，返回下标

//在顺序表的pos位置进行更改操作，pos位置可由find函数找出
void SQLmodify(SeqList* sql, size_t pos, size_t x);
