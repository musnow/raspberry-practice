﻿#define _CRT_SECURE_NO_WARNINGS 1

#include"Seqlist.h"
#define M 0

void test1(SeqList* sql)
{
	SQLpushback(sql, 1);
	SQLpushback(sql, 2);
	SQLpushback(sql, 3);
	SQLpushback(sql, 4);
	SQLprint(sql);

	SQLpopback(sql);
	SQLprint(sql);

	SQLpushfront(sql, 5);
	SQLprint(sql);

	SQLpushback(sql, 4);
	SQLprint(sql);

	SQLpopfront(sql);
	SQLprint(sql);

	SQLinsert(sql, 3, 10);
	SQLprint(sql);

	//SQLerase(sql, 3);
	//SQLprint(sql);

	int n = SQLfind(sql, 10);
	printf("%d\n", n);
	SQLmodify(sql, n, 7);
	SQLprint(sql);

	return;
}

void menu()
{
	printf("*****************************\n");
	printf("******1.头插  2.尾插*********\n");
	printf("******3.头删  4.尾删*********\n");
	printf("******5.插入  6.删除*********\n");
	printf("******7.查找  8.更改*********\n");
	printf("******9.打印  0.exit*********\n");
	printf("*****************************\n");

}



int main()
{
	SeqList s;//创建结构体变量
	SQLinst(&s);//初始化
#if	M
	test1(&s);//测试函数
#endif	
	menu();
	printf("请输入命令>");
	int option;
	scanf("%d", &option);
	do{
		switch (option)
		{
		case 0:
		{
			SQLdestory(&s);
			printf("destory SQL\n");
			exit(0);
		}
		case 1:
		{
			int x;
			printf("请输入需要头插的数>");
			scanf("%d", &x);
			SQLpushfront(&s, x);
			break;
		}
		case 2:
		{
			int y;
			printf("请输入需要尾插的数>");
			scanf("%d", &y);
			SQLpushback(&s, y);
			break;
		}
		case 3:
			SQLpopfront(&s);
			break;
		case 4:
			SQLpopback(&s);
			break;
		case 5:
		{
			int m, n;
			printf("请输入插入位置和插入数>");
			scanf("%d %d", &m, &n);
			SQLinsert(&s, m, n);
			break;
		}
		case 6:
		{
			int d;
			printf("请输入待删除数的位置>");
			scanf("%d", &d);
			SQLerase(&s, d);
			break;
		}
		case 7:
		{
			int f;
			printf("请输入需要查找的数>");
			scanf("%d", &f);
			printf("该数下标为:%d\n", SQLfind(&s, f));
			break;
		}
		case 8:
		{
			int h, i;
			printf("请输入需要更改的下标和新的数字\n");
			printf("如果您不知道该数的位置，可以调用查找模块\n");
			printf("请输入>");
			scanf("%d %d", &h, &i);
			SQLmodify(&s, h, i);
			break;
		}
		case 9:
			SQLprint(&s);
			break;
		}
		printf("请输入命令>");
	} while (scanf("%d", &option) != EOF);

	return 0;
}