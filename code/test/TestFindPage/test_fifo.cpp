#include <deque>
#include <iostream>
#include <algorithm>

int main()
{
    std::deque<int> dq;
    int n, num_page, num_lost_page = 0;
    std::cout << "[正在使用fifo算法]" << std::endl;
    std::cout << "请输入物理页框块数> ";
    std::cin >> num_page;
    std::cout << "请输入页面走向个数> ";
    std::cin >> n;
    for (int i = 0; i < n; i++)
    {
        int in;
        std::cout << "请输入当前页面: ";
        std::cin >> in;
        if (dq.size() < num_page) // 存在空余页框
        {
            bool is_in = false;
            for (auto &pos : dq) // 遍历队列
            {
                if (pos == in)
                {
                    is_in = true; // 当前页面已经在队列里面了。
                    break;
                }
            }

            if (!is_in) // 不存在此页面
            {
                num_lost_page++;  // 缺页次数加一
                dq.push_back(in); // 放入队列
            }
        }
        else // 不存在多余页框
        {
            bool is_in = false;
            for (auto &pos : dq) // 遍历队列
            {
                if (pos == in)
                {
                    is_in = true;
                    break;
                } // 存在该元素
            }

            if (!is_in) // 不存在此元素 则置换最先进入的项
            {
                num_lost_page++;  // 缺页数+1
                dq.pop_front();   // 最先进入的出队列
                dq.push_back(in); // 将当前需要的页面进队列
            }
        }
    }
    std::cout << std::endl;
    std::cout << "fifo缺页次数为: " << num_lost_page << std::endl;
    std::cout << "fifo缺页率为：" << ((num_lost_page * 1.0 / n)*100) << "%" << std::endl;
}
