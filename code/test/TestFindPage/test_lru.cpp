#include <deque>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <algorithm>

struct opt
{
    int value; // 值
    int time;  // 存放最后一次被访问的时间，可以用遍历时的下标来标识
};

int main()
{
    std::vector<opt> dq; // lru算法不需要用queue
    int n, num_page, num_lost_page = 0;
    std::cout << "[正在使用lru算法]" << std::endl;
    std::cout << "请输入物理页框块数> ";
    std::cin >> num_page;
    std::cout << "请输入页面走向个数> ";
    std::cin >> n;
    // 输入每个页的编号到数组中
    std::vector<int> num_array;
    for (int i = 0; i < n; i++)
    {
        int temp;
        std::cout << "请输入页面: ";
        std::cin >> temp;
        num_array.push_back(temp);
    }
    // 开始遍历处理
    int in;
    for (int i = 0; i < n; i++)
    {
        in = num_array[i];
        if (dq.size() < num_page) // 存在多余页框
        {
            bool is_in = false;
            for (auto &pos : dq) // 遍历队列
            {
                if (pos.value == in)
                {
                    is_in = true; // 当前页面已经在队列里面了。
                    pos.time = i; // 存放当前访问这个页面的下标
                    std::cout << "页面 " << in << " 已在主存中" << std::endl;
                    break;
                }
            }
            if (!is_in) // 不存在此元素
            {
                num_lost_page++;
                opt temp; // 构造新的页面，插入进去
                temp.value = in;
                temp.time = i;
                dq.push_back(temp);
            }
        }
        else // 不存在多余页框
        {
            bool is_in = false;
            for (auto &pos : dq) // 遍历队列
            {
                if (pos.value == in)
                {
                    is_in = true; // 当前页面已经在队列里面了。
                    pos.time = i; // 存放当前访问这个页面的下标
                    std::cout << "页面 " << in << " 已在主存中" << std::endl;
                    break;
                }
            }
            if (!is_in) // 不存在此元素
            {
                num_lost_page++; // 缺页数+1
                int time_max = 0;
                auto erase_temp = dq.begin();                       // 即将需要被删除的页面
                for (auto pos = dq.begin(); pos != dq.end(); pos++) // 遍历队列
                {
                    int diff = i - pos->time;
                    if (diff > time_max)
                    {
                        time_max = diff;
                        erase_temp = pos;
                    }
                }
                std::cout << "队列中最长没有被使用的页面为：" << erase_temp->value << std::endl;
                dq.erase(erase_temp);
                // 删除页面后，插入新的
                opt temp; // 构造新的页面，插入进去
                temp.value = in;
                temp.time = i;
                dq.push_back(temp);
            }
        }
        std::cout << "队列中已有页面如下" << std::endl;
        for (auto &pos : dq) // 遍历队列
        {
            std::cout << "页面号：" << pos.value << " 上次访问距离现在时间：" << (i - pos.time) << std::endl;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "lru缺页次数为: " << num_lost_page << std::endl;
    std::cout << "lru缺页率为：" << ((num_lost_page * 1.0 / n) * 100) << "%" << std::endl;
}
