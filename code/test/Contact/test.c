#define _CRT_SECURE_NO_WARNINGS 1
//这是一个测试
#include"contact.h"

//通讯录可以用来存储1000个人的信息
//每个人的信息包括：姓名、性别、年龄、电话、住址
//
//1.添加联系人信息
//2.删除指定联系人信息
//3.查找指定联系人信息
//4.修改指定联系人信息
//5.显示所有联系人信息
//6.清空所有联系人
//7.以名字排序所有联系人


//利用枚举变量使选项更清晰
enum option
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SHOW,
	REFRESH,
	SORT
};

void menu()
{
	printf("*********************************\n");
	printf("****** 1.add     2.del     ******\n");
	printf("****** 3.search  4.modify  ******\n");
	printf("****** 5.show    6.refresh ******\n");
	printf("****** 7.sort    0.exit    ******\n");
	printf("*********************************\n");
}


int main()
{
	int input;
	Contact con;
	Initial(&con);
	do 
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			AddContact(&con);
			//clear();
			break;
		case DEL:
			DelContact(&con);
			//clear();
			break;
		case SEARCH:
			SearchContact(&con);
			//clear();
			break;
		case MODIFY:
			ModifyContact(&con);
			//clear();
			break;
		case SHOW:
			ShowContact(&con);
			//clear();
			break;
		case REFRESH:
			Initial(&con);
			printf("已清空通讯录\n");
			//clear();
			break;
		case SORT:
			SortContact(&con);
			//clear();
			break;
		case EXIT:
			SaveContact(&con);
			DestroyContact(&con);
			printf("退出通讯录\n");
			break;
		default:
			printf("输入有误\n");
			//clear();
			break;
		}

	} while (input);
	


	return 0;
}
