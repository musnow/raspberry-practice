#include <iostream>
#include <string>
#include <cstdlib>
#include <cassert>
#include <unistd.h>
#include <strings.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
using namespace std;

struct sockaddr_in server;

void *recverAndPrint(void *args)
{
    while (true)
    {
        int sockfd = *(int *)args;
        char buffer[1024];
        struct sockaddr_in temp;
        socklen_t len = sizeof(temp);
        ssize_t s = recvfrom(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr *)&temp, &len);
        if (s > 0)
        {
            buffer[s] = 0;//将接收到的信息打印出来（服务器返回的）
            cout << "server echo# " << buffer << "\n";
        }
    }
}

// ./udpClient server_ip server_port
// 客户端要连接server，必须知道server对应的ip和port
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        cout << "Usage:\n\t" << argv[0] << " server_ip server_port" << endl;
        return 1;
    }
    // 1. 根据命令行，设置要访问的服务器IP
    string server_ip = argv[1];
    uint16_t server_port = atoi(argv[2]);

    // 2. 创建客户端
    // 2.1 创建socket
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd<0)
    {
        cout << "socket 创建失败" << endl;
        return 2;
    }

    bzero(&server, sizeof(server));//这个函数相当于memset全0

    server.sin_family = AF_INET;//ipv4
    server.sin_port = htons(server_port);//目标服务器端口
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());//目标ip


    // 使用多线程操作，来获取服务器传回的信息
    pthread_t t;
    pthread_create(&t, nullptr, recverAndPrint, (void *)&sockfd);

    // 3. 通讯过程
    string buffer;
    while (true)
    {
        cerr << "Please Enter# ";
        getline(cin, buffer);
        // 发送消息给server
        sendto(sockfd, buffer.c_str(), buffer.size(), 0,
               (const struct sockaddr *)&server, sizeof(server)); 
        // 首次调用sendto函数的时候，client会自动bind自己的ip和port
        // 客户端不应该自己绑定端口，否则端口被占用=客户端不能用
        usleep(200);
    }
    close(sockfd);

    return 0;
}