#pragma once

#include <iostream>
#include <queue>
#include <assert.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/prctl.h> //用于更改ps -aL的线程名字
using namespace std;

#define DEFALUT_NUM 5

template <class T>
class ThreadPool{
private:
    ThreadPool(int num = DEFALUT_NUM)
        : _threadNum(num),
          _isStart(false)
    {
        assert(num > 0);
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }
    ThreadPool(const ThreadPool<T> &) = delete;//取消拷贝
    void operator=(const ThreadPool<T> &) = delete;//取消赋值
public:
    static ThreadPool<T> *getInstance(int thread_num = DEFALUT_NUM)
    {
        static pthread_mutex_t mt;//使用static，只会创建一次；避免多次实例化，一个执行流一个锁，失去效果
        pthread_mutex_init(&mt,nullptr);
        if (instance == nullptr) // 第一次判断
        {
            pthread_mutex_lock(&mt);// 加锁，保证只有一个执行流走到这里
            if (instance == nullptr)// 第二次判断是来确认的，避免出现在加锁前，被其他执行流获取过实例了
            {
                instance = new ThreadPool<T>(thread_num);// 确认是null，创建单例
            }
        }

        pthread_mutex_unlock(&mt);
        pthread_mutex_destroy(&mt);
        return instance;
    }
    // 使用static无须用this指针调用
    static void *threadRoutine(void *args)
    {
        ThreadPool<T> *tp = static_cast<ThreadPool<T> *>(args);//c++强转
        prctl(PR_SET_NAME, "handler");//修改线程名字
        while (1)
        {
            tp->lockQueue();
            while (!tp->haveTask())
            {
                tp->waitForTask();
            }
            // 任务被拿到了线程的上下文中
            T t = tp->pop();
            tp->unlockQueue();

            // 规定每一个封装的task对象都需要有一个run函数
            t();
            //t.resultPrint(t.run());//运行并打印结果
        }
    }
    void start()
    {
        assert(!_isStart);//如果开启了，那么就不能执行该函数
        for (int i = 0; i < _threadNum; i++)
        {
            pthread_t temp;
            pthread_create(&temp, nullptr, threadRoutine, this);//把this当参数传入
            usleep(100);
            pthread_detach(temp);//分离线程
        }
        _isStart = true;//标识状态，代表线程池已经启动了
    }
    //往线程池中给任务
    void push(const T &in)
    {
        lockQueue();
        _tq.push(in);//插入任务
        singalThread();//任务插入后，唤醒一个线程来执行
        unlockQueue();
    }

    ~ThreadPool()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_cond);
    }
private:
    void lockQueue() { pthread_mutex_lock(&_mutex); }
    void unlockQueue() { pthread_mutex_unlock(&_mutex); }
    bool haveTask() { return !_tq.empty(); }
    void waitForTask() { pthread_cond_wait(&_cond, &_mutex); }
    void singalThread() { pthread_cond_signal(&_cond); }
    T pop()
    {
        T temp = _tq.front();
        _tq.pop();
        return temp;
    }
private:
    bool _isStart;  // 线程池子是否启动
    int _threadNum; // 线程数量
    queue<T> _tq;   // 任务队列
    pthread_mutex_t _mutex;// 锁
    pthread_cond_t _cond;  // 条件变量

    static ThreadPool<T> *instance; // 单例模式需要用到的指针
};
// 初始化static变量
template <class T>
ThreadPool<T> *ThreadPool<T>::instance = nullptr;