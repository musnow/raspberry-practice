#pragma once

#include <iostream>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h> 
#include <fcntl.h> // O_RDWR 需要

void daemonize()
{
    int fd = 0;
    // 1. 忽略SIGPIPE (管道读写，读端关闭，写端会收到信号终止)
    signal(SIGPIPE, SIG_IGN);
    // 2. 更改进程的工作目录
    // chdir(); // 可以改，可以不改
    // 3. 让自己不要成为进程组组长
    if (fork() > 0)
        exit(0);
    // 4. 设置自己是一个独立的会话
    setsid();
    // 5. 重定向0,1,2
    if ((fd = open("/dev/null", O_RDWR)) != -1) // fd == 3
    {
        dup2(fd, STDIN_FILENO);
        dup2(fd, STDOUT_FILENO);
        dup2(fd, STDERR_FILENO);
        // 6. 关闭掉不需要的fd
        // 因为fd只是临时用于重定向，操作完毕了就可以关掉了
        if(fd > STDERR_FILENO) 
            close(fd);
    }
    // 这里还有另外一种操作，就是把stdin/stdout/stderr给close了
    // 但是这样会导致只要有打印输出的代码，进程会就异常退出
}