#include <iostream>
#include <future>  
#include <thread>
#include <sys/unistd.h>
using namespace std;

int add (int a,int b)
{
    std::cout << "ADD Thread " << std::this_thread::get_id()<< std::endl;
    sleep(5);
    return a+b;
}

void future_get_func_shared(std::shared_future<int>& fu)
{
    int result = fu.get();  // 等待异步任务完成并获取结果
    std::cout << "Func Thread " << std::this_thread::get_id() << " | Result: " << result << std::endl;
    sleep(2);
}

void future_get_func(std::future<int>& fu)
{
    int result = fu.get();  // 等待异步任务完成并获取结果
    std::cout << "Func Thread " << std::this_thread::get_id() << " | Result: " << result << std::endl;
    sleep(2);
}
// int main()
// {
//     // ret = add(3,4);
//     future<int> future_ret = async(launch::async,add,3,4);
//     // cout << "main: " << future_ret.valid() << endl;
    
//     auto shared_fu = future_ret.share();
//     // thread t1(future_get_func_shared,std::ref(shared_fu));
//     // t1.detach();

//     sleep(2);

//     cout << "main Thread "<< std::this_thread::get_id() <<  "  | " << shared_fu.valid() << endl;
//     int result = shared_fu.get();
//     cout  << "main Thread "<< std::this_thread::get_id() << " | " << result << endl;

//     sleep(5);
    
//     return 0;
// }


void worker(std::promise<int>& p) 
{
   try {
    // 产生异常
    throw std::runtime_error("An error occurred");
    } catch (...) {
        // 将异常设置到 promise 对象中
        p.set_exception( current_exception());
    }
}

int main() 
{
    try{
    std::promise<int> promiseObj;
    std::future<int> futureResult = promiseObj.get_future();

    cout << "main Thread "<< std::this_thread::get_id() << endl;
    std::thread t(worker, std::ref(promiseObj)); // 通过线程执行
    t.join();

    int result = futureResult.get(); // 主线程中获取值
    std::cout << "main Thread "<< std::this_thread::get_id() << "Result: " << result << std::endl;

    }
    catch(...)
    {
        std::cerr <<  "exception occur\n";
    }
    

    return 0;
}