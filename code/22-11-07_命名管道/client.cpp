#include"MyPath.h"

int main()
{
    int pipeFd = open(FILE_PATH, O_WRONLY);
    if(pipeFd < 0)
    {
        cerr << "open: " << strerror(errno) << endl;
        return 1;
    }
    //客户端执行管道写入
    cout<<"客户端启动"<<endl;
    char line[NUM];
    while(true)
    {
        printf("请输入你的消息# ");
        fflush(stdout);
        memset(line, 0, sizeof(line));
        //fgets的结尾会自动添加\0
        if(fgets(line, sizeof(line), stdin) != nullptr)
        {
            //这里的意义是去掉接收到的回车\n
            //如：abcde\n\0 通过下面的代码去掉\n
            line[strlen(line) - 1] = '\0';
            write(pipeFd, line, strlen(line));//管道写入
        }
        else
        {
            break;
        }
    }
    close(pipeFd);
    cout << "客户端退出" << endl;
    return 0;
}