#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

// int main()
// {
//     //获取进程编号
//     while(1){
//         printf("pid: %d  ", getpid());
//         printf("ppid: %d\n", getppid());
//         sleep(1);//睡1s
//     }
    
//     return 0;
// }

int main()
{
    pid_t id = fork();
    //id=0 子进程；>0为父进程
    if(id == 0)
    {
        //child
        while(1)
        {
            printf("子进程，pid: %d, 父进程是: %d\n", getpid(), getppid());
            sleep(1);
        }
    }
    else{
        //parent
        while(1)
        {
            printf("父进程，pid: %d, 父进程是: %d\n", getpid(), getppid());
            sleep(1);
        }
    }
}