#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/unistd.h>

int main() {
    int semid;
    key_t key;
    struct sembuf semaphore;

    // 创建或获取信号量集
    key = ftok(".", 'S');
    semid = semget(key, 1, IPC_CREAT | 0666);
    if (semid == -1) {
        perror("Failed to create semaphore\n");
        exit(1);
    }
    printf("1\n");

    // 初始化信号量的值为1
    if (semctl(semid, 0, SETVAL, 1) == -1) {
        perror("Failed to initialize semaphore value\n");
        exit(1);
    }
    printf("12\n");


    // 对信号量进行操作,必须要先加才能减
    semaphore.sem_num = 0;
    semaphore.sem_op = 34;  // 新增信号量值
    semaphore.sem_flg = SEM_UNDO;
    if (semop(semid, &semaphore, 1) == -1) {
        perror("Failed to perform semaphore operation\n");
        exit(1);
    }
    printf("13\n");

    // 获取信号量的当前值
    int cmd = GETVAL;  // 获取信号量的命令，GETVAL表示获取当前值
    int sem_value = semctl(semid, 0, cmd);
    if(sem_value == -1)
    {
        perror("Failed to perform semaphore operation\n");
        exit(1);
    }
    else{
        printf("current val for sem: %d\n",sem_value);
    }
    printf("14\n");


    // 释放信号量
    semaphore.sem_op = -34;  // 减少信号量值
    if (semop(semid, &semaphore, 1) == -1) {
        perror("Failed to release semaphore\n");
        exit(1);
    }
    printf("15\n");

    // 删除信号量集
    if (semctl(semid, 0, IPC_RMID) == -1) {
        perror("Failed to remove semaphore\n");
        exit(1);
    }
    printf("16\n");

    return 0;
}
