#include "Mykey.hpp"

int main()
{
    //创建管道
    CreateFifo();
    //获取key值
    key_t key = CreateKey();
    //创建共享内存
    int id = shmget(key, NUM, IPC_CREAT | IPC_EXCL | 0666);
    if(id<0)
    {
        cerr<< "shmget err: " << strerror(errno) << endl; 
        return 1;
    }
    cout << "shmget success: " << id << endl;
    //获取管道
    int fd = Open(O_RDONLY);
    cout << "open fifo success: " << fd << endl;
    sleep(2);
    //关联共享内存
    char *str = (char*)shmat(id, nullptr, 0);
    printf("[server] shmat success\n");
    //读取数据
    int i=0;
    while(i<=40)
    {
        ssize_t ret = Wait(fd);//通过管道等待
        if(ret!=0)
        {
            printf("[%03d] %s\n",i,str);
            i++;
            sleep(1);
        }
        else
        {
            cout<<"[server] wait finish, break" << endl;
            break;
        }
    }

    //去关联
    shmdt(str);//shmat的返回值
    printf("[server] shmdt(str)\n");
    //删除共享内存
    shmctl(id,IPC_RMID,nullptr);
    close(fd);
    unlink(FIFO_FILE);
    printf("[server] exit\n");
    return 0;
}