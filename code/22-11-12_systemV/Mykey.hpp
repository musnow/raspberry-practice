#pragma once

#include <iostream>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <cstdlib>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <unistd.h>
#include <cassert>
using namespace std;

#define NUM 1024
#define PROJ_ID 0x20
#define PATH_NAME "/home/muxue/git/linux/code/22-11-12_systemV"
#define FIFO_FILE "sc.pipe"

key_t CreateKey()
{
    key_t key = ftok(PATH_NAME, PROJ_ID);
    if(key < 0)
    {
        cerr <<"ftok: "<< strerror(errno) << endl;
        exit(1);//key获取错误直接退出程序
    }
    return key;
}

void CreateFifo()
{
    umask(0);
    if(mkfifo(FIFO_FILE, 0666) < 0)
    {
        cerr << "fifo: " << strerror(errno) << endl;
        exit(2);
    }
}
//打开管道文件
int Open(int flags)
{
    return open(FIFO_FILE, flags);
}
//让读端通过管道等待
ssize_t Wait(int fd)
{
    char val = 0;
    //如果写端没有写入，其就会在read中等待
    ssize_t s = read(fd, &val, sizeof(val));
    return s;
}
//发送完成信息
int Signal(int fd)
{
    char sig = 'g';
    write(fd, &sig, sizeof(sig));
}