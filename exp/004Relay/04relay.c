#include <wiringPi.h>
#include <stdio.h>

#define makerobo_RelayPin 0 //设置继电器GPIO管脚，0对应G17

int main()
{
    //初始化连接失败时，将消息打印到屏幕	
	if(wiringPiSetup() == -1){ 
		printf("setup wiringPi failed !");
		return 1; 
	}	
	//将继电器管脚设置为输出模式
	pinMode(makerobo_RelayPin, OUTPUT);
	
	int k=2;
	while(k--)
	{
			digitalWrite(makerobo_RelayPin, HIGH); // 打开继电器
			delay(1000);                           // 延时1s		
			digitalWrite(makerobo_RelayPin, LOW);  // 关闭继电器			
			delay(1000);                           // 延时1s
	}
	return 0;
}

