//PCF8591模数转换器
#include <stdio.h>
#include <wiringPi.h>
#include <pcf8591.h>//这个头文件是包含在库中的

#define makerobo_PCF       120  // 基础管脚120

// 主函数
int main ()
{
	int pcf_value ;       // 定义一个变量存储AIN的输入值

	//初始化连接失败时，将消息打印到屏幕
	if(wiringPiSetup() == -1){ 
		printf("setup wiringPi failed !");
		return 1; 
	}

	pcf8591Setup (makerobo_PCF, 0x48) ;// 在基本引脚120上设置pcf8591，地址0x48

	while(1) // 无限循环
	{
		pcf_value = analogRead  (makerobo_PCF + 0) ;
		// 获取AIN0上的值，插上跳线帽之后，采用的是内部的电位器；
		printf("%d\n", pcf_value);                  // 打印出该值            
		analogWrite (makerobo_PCF + 0, pcf_value) ; // 控制AOUT输出电平控制LED灯
		delay (10) ;                                // 延时10ms
	} 
	return 0 ;
}
